package io.gnews.pro.web.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.base.Throwables;
import com.paypal.api.payments.Links;
import com.paypal.base.rest.PayPalRESTException;

import io.gnews.pro.core.model.mongodb.Payment;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.repository.mongodb.ProjectRepository;
import io.gnews.pro.web.config.AppConfig;
import io.gnews.pro.web.config.paypal.PaypalPaymentIntent;
import io.gnews.pro.web.config.paypal.PaypalPaymentMethod;
import io.gnews.pro.web.request.PaymentMethodForm;
import io.gnews.pro.web.request.PaymentOrderForm;
import io.gnews.pro.web.service.PaymentService;
import io.gnews.pro.web.service.PaypalService;
import io.gnews.pro.web.service.ProjectService;
import io.gnews.pro.web.service.SiteService;
import io.gnews.pro.web.util.URLUtils;

@Controller
@RequestMapping(value="/payment")
public class PaymentController {
	
	public static final String PAYPAL_SUCCESS_URL = "paypal/success";
	public static final String PAYPAL_CANCEL_URL = "paypal/cancel";
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private SiteService siteService;
		
	@Autowired
	private PaymentService paymentService;

	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired
	private PaypalService paypalService;
	
	/*@RequestMapping(method = RequestMethod.GET, value = "/order")
	public String paymentOrder(
			HttpSession session, 
			Principal principal, 
			Model model){	
		if (principal == null) {
			return "redirect:/";
		}
		try{
			UserAccount userAccount = userAccountRepository.findByEmail(principal.getName());
			String projectId = null;
			if(session.getAttribute(AppConfig.SESSION_DATA_PROJECT_ID) != null){
				projectId = (
						session.getAttribute(AppConfig.SESSION_DATA_PROJECT_ID).toString());
			}
			
			String paymentId = null;
			if(session.getAttribute(AppConfig.SESSION_DATA_PAYMENT_ID) != null){
				paymentId = (
						session.getAttribute(AppConfig.SESSION_DATA_PAYMENT_ID).toString());
			}
			if(projectId == null){
				return "redirect:/project";
			}
			
			if(userAccount.getAgent() == null){
				AgentAccount agentAccount = agentAccountRepository.findByEmail("admin@gnews.io");
				if(agentAccount != null){
					model.addAttribute("agentPrice", agentAccount.getDefaultPrice());						
				}else{
					model.addAttribute("agentPrice", AppConfig.DEFAULT_PRICE);	
				}
			}else{
				model.addAttribute("agentPrice", userAccount.getAgent().getDefaultPrice());
			}
			
			siteService.setSiteInfo(principal, model);
			model.addAttribute("project", projectService.getById(projectId));
			model.addAttribute("payment", paymentService.getByid(paymentId));
			return "user/order-detail";
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
			return "redirect:/project";
		}
		
	}*/

	@RequestMapping(method = RequestMethod.POST, value = "/order/save")
	public String savePaymentOrder(
			Principal principal,
			HttpSession session, 
			Model model,
			@ModelAttribute PaymentOrderForm form){	
		if (principal == null) {
			return "redirect:/";
		}
		try{
			Payment payment = paymentService.savePaymentOrder(principal, session, form);
			String projectId = null;
			if(session.getAttribute(AppConfig.SESSION_DATA_PROJECT_ID) != null){
				projectId = (
						session.getAttribute(AppConfig.SESSION_DATA_PROJECT_ID).toString());
				Project project = projectRepository.findOne(projectId);
				if(project.isRecurrent() || payment.getTotal() == 0){
					return "redirect:/project";
				}
			}			
			return "redirect:/payment/method";
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
			return "redirect:/project";
		}
	}
	
	@RequestMapping(method=RequestMethod.GET, value = "/method")
	public String paymentMethod(
			HttpSession session, 
			Principal principal, 
			Model model){
		if(principal == null){
			return "redirect:/";
		}
		try{
			String projectId = null;
			if(session.getAttribute(AppConfig.SESSION_DATA_PROJECT_ID) != null){
				projectId = (
						session.getAttribute(AppConfig.SESSION_DATA_PROJECT_ID).toString());
			}
			
			String paymentId = null;
			if(session.getAttribute(AppConfig.SESSION_DATA_PAYMENT_ID) != null){
				paymentId = (
						session.getAttribute(AppConfig.SESSION_DATA_PAYMENT_ID).toString());
			}
			siteService.setSiteInfo(principal, model);
			model.addAttribute("project", projectService.getById(projectId));
			model.addAttribute("payment", paymentService.getByid(paymentId));
			return "user/order-billing";
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
			return "redirect:/project";
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/method/save")
	public String savePaymentMethod(
			HttpServletRequest request,
			HttpSession session, 
			Principal principal, 
			@ModelAttribute PaymentMethodForm Form) {
		if (principal == null) {
			return "redirect:/";
		}
		try{
			Payment payment = paymentService.savePaymentMethod(principal, session, Form);
			if(payment.getMethod().equalsIgnoreCase(AppConfig.PAYMENT_METHOD_PAYPAL)){
				String cancelUrl = URLUtils.getBaseURl(request) + "/" + PAYPAL_CANCEL_URL;
				String successUrl = URLUtils.getBaseURl(request) + "/" + PAYPAL_SUCCESS_URL;
				try {
					com.paypal.api.payments.Payment paypalPayment = paypalService.createPayment(
							payment.getTotal().doubleValue(), 
							"USD", 
							PaypalPaymentMethod.paypal, 
							PaypalPaymentIntent.sale,
							payment.getInvoice(), 
							cancelUrl, 
							successUrl);
					for(Links links : paypalPayment.getLinks()){
						if(links.getRel().equals("approval_url")){
							return "redirect:" + links.getHref();
						}
					}
				} catch (PayPalRESTException e) {
					log.error(e.getMessage());
				}
			}
			return "redirect:/payment/invoice/"+payment.getInvoice();
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
			return "redirect:/project";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value ="/invoice/{invoice}")
	public String paymentStatus(
			HttpSession session, 
			Principal principal, 
			Model model,
			@PathVariable String invoice){
		if (principal == null){
			return "redirect:/";
		}
		try{
			siteService.setSiteInfo(principal, model);
			model.addAttribute("payment", paymentService.getByInvoice(invoice));
			return "user/payment-stat";
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
			return "redirect:/project";
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = PAYPAL_CANCEL_URL)
	public String cancelPay(){
		return "redirect:/payment/invoice/";
	}

	@RequestMapping(method = RequestMethod.GET, value = PAYPAL_SUCCESS_URL)
	public String successPay(@RequestParam("paymentId") String paymentId, @RequestParam("PayerID") String payerId){
		try {
			com.paypal.api.payments.Payment paypalPayment = paypalService.executePayment(paymentId, payerId);
			if(paypalPayment.getState().equals("approved")){
				Payment payment = paymentService.getByInvoice(paypalPayment.getTransactions().get(0).getDescription());
				if(payment != null){
					paymentService.setPaid(payment);
					return "redirect:/payment/invoice/"+payment.getInvoice();
				}
			}
		} catch (PayPalRESTException e) {
			log.error(e.getMessage());
		}
		return "redirect:/project";
	}
	
}
