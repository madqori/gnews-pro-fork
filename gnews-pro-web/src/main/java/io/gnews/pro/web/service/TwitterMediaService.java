package io.gnews.pro.web.service;

import java.util.Date;

public interface TwitterMediaService {
	
	public void getWebdata(String id, String link, Date date);
		
	public boolean isLanguageSupported(String languageCode);
	
}
