package io.gnews.pro.web.service;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import io.gnews.pro.core.model.dto.MessageSimple;
import io.gnews.pro.core.model.mongodb.EmailGroup;
import io.gnews.pro.core.model.mongodb.EmailMessage;
import io.gnews.pro.core.model.mongodb.EmailSender;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.model.mongodb.EmailRecipient;
import io.gnews.pro.web.request.AddEmailRecipient;
import io.gnews.pro.web.request.AddEmailSenderForm;
import io.gnews.pro.web.request.ComposeMailForm;
import io.gnews.pro.web.request.CreateGroupForm;

public interface MailMarketingService {

	public void addSender(HttpServletRequest request, Principal principal, Project project, AddEmailSenderForm form);
	
	public List<EmailMessage> getBlastData(Project project, Integer page, Integer size);
	
	public List<EmailGroup> getMailGroups(Project project);

	public EmailMessage getBlastDetail(String messageId);

	public void saveMailBlast(Principal principal, Project project, ComposeMailForm form);

	public EmailGroup getMailGroup(String groupId);

	public List<EmailRecipient> getMailRecipients(String groupId, Integer page, Integer size);

	public void deleteGroup(Principal principal, String groupId);

	public void addReceipient(Principal principal, String groupId, AddEmailRecipient form);

	public void deleteReceipient(Principal principal, String groupId, String receiverId);

	public List<EmailSender> getEmailSender(String projectId);

	public void deleteSender(Principal principal, Project project, String senderId);

	public void saveMailBlast(MessageSimple message);

	public void addGroupMail(Principal principal, Project project, CreateGroupForm form);

	public List<EmailSender> getValidEmailSender(Project project);

	public void importContact(EmailGroup group, MultipartFile file);

	public void editSender(Principal principal, Project project, AddEmailSenderForm form);

	public void unsubscribeReceipient(HttpServletRequest request, String groupId, String recipientId);

	public String generateUnsubscribeUrl(HttpServletRequest request, String groupId, String recipientId);

}
