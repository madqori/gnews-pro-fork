package io.gnews.pro.web.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerMapping;

import io.gnews.pro.web.service.UploadService;
import io.gnews.pro.web.service.impl.UserServiceImpl;

@Controller
@RequestMapping("/images")
public class ImageController {

	@Value("${report.data.companylogo}")
	private String companyLogoDir;
	
	@Value("${upload.data.dir}")
	private String dataDir;
	
	Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/uploads/**")
	public void getProductImage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("image/png");
		String fileUrl = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		String filename = fileUrl.replace("/images/uploads/", "");
		String filePath = dataDir + UploadService.UPLOAD_DIR + UploadService.IMAGE_DIR  + UserServiceImpl.BASE_IMAGE_DIR + filename;
		File image = new File(filePath);
		InputStream in = FileUtils.openInputStream(image);
		IOUtils.copy(in, response.getOutputStream());
	}

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/logos/**")
	public void getLogoImage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("image/png");
		String fileUrl = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		String filename = fileUrl.replace("/images/logos/", "");
		String filePath = companyLogoDir + filename;
		File image = new File(filePath);
		InputStream in = FileUtils.openInputStream(image);
		IOUtils.copy(in, response.getOutputStream());
	}
}