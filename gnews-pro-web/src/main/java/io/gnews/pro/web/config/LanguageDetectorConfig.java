package io.gnews.pro.web.config;

import java.io.IOException;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.optimaize.langdetect.LanguageDetector;
import com.optimaize.langdetect.LanguageDetectorBuilder;
import com.optimaize.langdetect.ngram.NgramExtractors;
import com.optimaize.langdetect.profiles.LanguageProfile;
import com.optimaize.langdetect.profiles.LanguageProfileReader;
import com.optimaize.langdetect.text.CommonTextObjectFactories;
import com.optimaize.langdetect.text.TextObjectFactory;

/**
 * config class for language detection
 * 
 * @author masasdani
 * Created Date Oct 27, 2015
 */
@Configuration
public class LanguageDetectorConfig {

	@Bean
	public LanguageDetector languageDetector(){
		List<LanguageProfile> languageProfiles;
		try {
			languageProfiles = new LanguageProfileReader().readAllBuiltIn();
			LanguageDetector languageDetector = LanguageDetectorBuilder.create(NgramExtractors.standard())
			        .withProfiles(languageProfiles)
			        .build();
			return languageDetector;
		} catch (IOException e) {
			return null;
		}
	}

	@Bean
	public TextObjectFactory textObjectFactory() {
		return CommonTextObjectFactories.forDetectingOnLargeText();
	}

}
