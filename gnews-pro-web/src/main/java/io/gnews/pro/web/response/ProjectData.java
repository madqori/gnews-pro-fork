package io.gnews.pro.web.response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.gnews.pro.core.model.mongodb.Project;

/**
 * @author madqori
 *
 */
public class ProjectData {
	
	private String id;
	private String name;
	private String email;
	private String type;
	private String filter;
	private String keyword;
	private Date createdAt;
	private Date expiredDate;
	private List<String> languages;
	private String projectStatus;
	private boolean active;
	private boolean expired;
	private int range;
	private int oldPeriod;
	
	public ProjectData() {
		
	}

	public ProjectData(Project project) {
		this.setId(project.getId());
		this.setName(project.getName());
		this.setActive(project.isActive());
		this.setKeyword(project.getKeyword());
		this.setFilter(project.getFilter());
		List<String> languages = new ArrayList<String>(project.getLanguanges());
		this.setLanguages(languages);
		this.setCreatedAt(project.getCreatedAt());
		this.setExpiredDate(project.getExpiredAt());
		this.setProjectStatus(project.getStatus());
		this.setExpired(project.isExpired());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}
	
	public List<String> getLanguages() {
		return languages;
	}

	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}

	public String getProjectStatus() {
		return projectStatus;
	}

	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public int getOldPeriod() {
		return oldPeriod;
	}

	public void setOldPeriod(int oldPeriod) {
		this.oldPeriod = oldPeriod;
	}
	
}
