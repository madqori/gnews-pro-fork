package io.gnews.pro.web.config;

import io.gnews.wispy.Wispy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WispyConfig {

	@Bean
	public Wispy wispy(){
		return new Wispy();
	}
	
}
