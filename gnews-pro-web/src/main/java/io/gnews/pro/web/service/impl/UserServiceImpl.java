package io.gnews.pro.web.service.impl;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.base.Throwables;

import io.gnews.pro.core.config.RabbitMQConfig;
import io.gnews.pro.core.model.dto.MailType;
import io.gnews.pro.core.model.dto.SendEmailForm;
import io.gnews.pro.core.model.mongodb.Inquiries;
import io.gnews.pro.core.model.mongodb.UserAccount;
import io.gnews.pro.core.repository.mongodb.InquiriesRepository;
import io.gnews.pro.core.repository.mongodb.UserAccountRepository;
import io.gnews.pro.web.exception.EmailRegisteredException;
import io.gnews.pro.web.exception.PasswordNotMatchException;
import io.gnews.pro.web.request.ChangePasswordForm;
import io.gnews.pro.web.request.InquiriesForm;
import io.gnews.pro.web.request.ProfileForm;
import io.gnews.pro.web.request.RegistrationForm;
import io.gnews.pro.web.response.ProfileData;
import io.gnews.pro.web.service.LoginService;
import io.gnews.pro.web.service.UploadService;
import io.gnews.pro.web.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	public static final String BASE_IMAGE_DIR = "profile/";
	public static final int IMAGE_SCALE_WIDTH = 400;
	public static final int IMAGE_SCALE_HEIGHT = 300;
	private static String EMAIL_SUPPORT ="info@gnews.id";
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private InquiriesRepository inquiriesRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UploadService uploadService;
	
	@Autowired
	private UserAccountRepository userAccountRepository;
	
	@Autowired
	private LoginService loginService;

	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Override
	public ProfileData getUserData(Principal principal) {
		UserAccount userAccount = userAccountRepository.findByEmail(principal.getName());
		ProfileData profileData = new ProfileData();
		profileData.setFullname(userAccount.getFullname());
		profileData.setAddress(userAccount.getAddress());
		if(userAccount.getProfilePicture() != null)
			profileData.setImage(userAccount.getProfilePicture());
		profileData.setCity(userAccount.getCity());
		profileData.setEmail(userAccount.getEmail());
		profileData.setPhone(userAccount.getPhone());
		profileData.setCompany(userAccount.getCompany());
		profileData.setPosition(userAccount.getPosition());
		return profileData;
	
	}
	@Override
	public void savePassword(ChangePasswordForm form, Principal principal) throws Exception {
		UserAccount account = userAccountRepository.findByEmail(principal.getName());
		if(passwordEncoder.matches(form.getOldPassword(),account.getPassword())){
			account.setPassword(passwordEncoder.encode(form.getNewPassword()));
			userAccountRepository.save(account);
		}else{
			throw new PasswordNotMatchException();
		}
	}
	
	@Override
	public void register(Principal principal, RegistrationForm form) throws EmailRegisteredException {
		UserAccount checkAccount = userAccountRepository.findByEmail(form.getEmail());
		UserAccount account = new UserAccount();
		if(checkAccount == null){
			account.setFullname(form.getName());
			account.setEmail(form.getEmail());
			account.setPhone(form.getPhone());
			account.setCompany(form.getCompany());
			account.setCreatedAt(new Date());
			account.setRecurrentUser(false);
			account.setActive(true);
			account.setRole(UserAccount.ROLE_USER);
			account.setPassword(passwordEncoder.encode(form.getPassword()));
			account = userAccountRepository.save(account);
			loginService.signin(account);
		}else{
			throw new EmailRegisteredException();
		}
	}
	
	@Override
	public void save(Principal principal, ProfileForm form, MultipartFile imageUpload) {
		UserAccount userAccount = userAccountRepository.findByEmail(principal.getName());
		userAccount.setFullname(form.getFullname());
		userAccount.setAddress(form.getAddress());
		userAccount.setCity(form.getCity());
		userAccount.setPhone(form.getPhone());
		userAccount.setCompany(form.getCompany());
		userAccount.setPosition(form.getPosition());
		
		if(!imageUpload.isEmpty()){
			try {
				String image = uploadService.uploadImageAndResize(
						BASE_IMAGE_DIR, 
						imageUpload.getOriginalFilename(),
						imageUpload, 
						IMAGE_SCALE_WIDTH, 
						IMAGE_SCALE_HEIGHT);
				if(image != null){
					userAccount.setProfilePicture(image);
					log.info("image change : " + image);
				}
			} catch (IOException e) {
				log.error(Throwables.getRootCause(e).getMessage());
			}
		}
		
		userAccountRepository.save(userAccount);
	}

	@Override
	public void reuestInquiry(InquiriesForm form) {
		Inquiries inquiries = new Inquiries();
		
		inquiries.setName(form.getName());
		inquiries.setEmail(form.getEmail());
		inquiries.setCompany(form.getCompany());
		inquiries.setPhone(form.getPhone());
		inquiries.setPosition(form.getPosition());
		inquiries.setMessage(form.getMessage());
		inquiries.setIndustries(form.getIndustries());
		inquiriesRepository.save(inquiries);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", form.getName());
		map.put("email", form.getEmail());
		map.put("company", form.getCompany());
		map.put("position", form.getPosition());
		map.put("message", form.getMessage());
		map.put("industries", form.getIndustries());

		SendEmailForm emailForm = new SendEmailForm();
		emailForm.setTo(EMAIL_SUPPORT);
		emailForm.setType(MailType.INQUIRY);
		emailForm.setData(map);

		rabbitTemplate.convertAndSend(RabbitMQConfig.SEND_MAIL_QUEUE, emailForm);
		log.info("inquiry sent");

	}
	
	@Override
	public UserAccount findUserByEmail(String email) {
		return userAccountRepository.findByEmail(email);
	}
	
}
