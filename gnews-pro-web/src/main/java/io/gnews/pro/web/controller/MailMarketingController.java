package io.gnews.pro.web.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.base.Throwables;

import io.gnews.pro.core.model.mongodb.EmailGroup;
import io.gnews.pro.core.model.mongodb.EmailMessage;
import io.gnews.pro.core.model.mongodb.EmailRecipient;
import io.gnews.pro.core.model.mongodb.EmailSender;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.web.request.AddEmailRecipient;
import io.gnews.pro.web.request.AddEmailSenderForm;
import io.gnews.pro.web.request.ComposeMailForm;
import io.gnews.pro.web.request.CreateGroupForm;
import io.gnews.pro.web.service.MailMarketingService;
import io.gnews.pro.web.service.ProjectService;
import io.gnews.pro.web.service.SiteService;

@Controller
@RequestMapping(value = "/project")
public class MailMarketingController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SiteService siteService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private MailMarketingService mailMarketingService;
	
	@RequestMapping(value = "/{projectId}/mail/start")
	public String mailMarketingStart(
			@PathVariable String projectId, 
			Model model,
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response, 
			Principal principal) {
		try {
			model.addAttribute("project", projectService.getById(projectId));
			siteService.setSiteInfo(principal, model);

			if (mailMarketingService.getEmailSender(projectId).size() >= 1) {
				return "redirect:/project/" + projectId + "/mail/group";
			} else {
				return "marketing-mail/marketing-mail-start";
			}

		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}

	@RequestMapping(value = "/{projectId}/mail/sender", method = RequestMethod.GET)
	public String mailconfig(
			@PathVariable String projectId, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			List<EmailSender> list = mailMarketingService.getEmailSender(projectId);
			Project project = projectService.getById(projectId);
			model.addAttribute("project", project);
			model.addAttribute("list", list);
			return "marketing-mail/sender-config-dashboard";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/sender";
	}

	@RequestMapping(value = "/{projectId}/mail/sender/add", method = RequestMethod.POST)
	public String addMailSender(
			@PathVariable String projectId, 
			@ModelAttribute AddEmailSenderForm form, 
			Model model, 
			HttpSession session,
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			Project project = projectService.getById(projectId);
			mailMarketingService.addSender(request, principal, project, form);
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/sender";
	}

	@RequestMapping(value = "/{projectId}/mail/sender/edit", method = RequestMethod.POST)
	public String editMailSender(
			@PathVariable String projectId, 
			@ModelAttribute AddEmailSenderForm form, 
			Model model, 
			HttpSession session,
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			Project project = projectService.getById(projectId);
			mailMarketingService.editSender(principal, project, form);
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/sender";
	}

	@RequestMapping(value = "/{projectId}/mail/sender/{senderId}/delete", method = RequestMethod.GET)
	public String editMailSender(
			@PathVariable String projectId, 
			@PathVariable String senderId, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			Project project = projectService.getById(projectId);
			mailMarketingService.deleteSender(principal, project, senderId);
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/sender";
	}

	@RequestMapping(value = "/{projectId}/mail/group", method = RequestMethod.GET)
	public String emailGroup(
			@PathVariable String projectId, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			Project project = projectService.getById(projectId);
			List<EmailGroup> list = mailMarketingService.getMailGroups(project);
			model.addAttribute("project", project);
			model.addAttribute("list", list);
			return "marketing-mail/groups-dashboard";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/group";
	}

	@RequestMapping(value = "/{projectId}/mail/group", method = RequestMethod.POST)
	public String addEmailGroup(
			@PathVariable String projectId, 
			@ModelAttribute CreateGroupForm form,
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			Project project = projectService.getById(projectId);
			mailMarketingService.addGroupMail(principal, project, form);
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/group";
	}
	
	@RequestMapping(value = "/{projectId}/mail/group/{groupId}", method = RequestMethod.GET)
	public String emailGroupDetail(
			@PathVariable String projectId, 
			@PathVariable String groupId, 
			@RequestParam(required=false) Integer page,
			@RequestParam(required=false) Integer size,
			Model model,
			HttpSession session, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			EmailGroup group = mailMarketingService.getMailGroup(groupId);
			List<EmailRecipient> receipients = mailMarketingService.getMailRecipients(groupId, page, size);
			Project project = projectService.getById(projectId);
			model.addAttribute("project", project);
			model.addAttribute("group", group);
			model.addAttribute("list", receipients);
			return "marketing-mail/groups-detail";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/group";
	}

	@RequestMapping(value = "/{projectId}/mail/group/{groupId}/import", method = RequestMethod.POST)
	public String addEmailGroupDetail(
			@PathVariable Long projectId, 
			@PathVariable String groupId,
			@RequestParam MultipartFile file,
			Model model,
			HttpSession session, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			EmailGroup group = mailMarketingService.getMailGroup(groupId);
			mailMarketingService.importContact(group, file);
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/group/" + groupId;
	}

	@RequestMapping(value = "/{projectId}/mail/group/{groupId}/delete", method = RequestMethod.GET)
	public String emailGroupDelete(
			@PathVariable Long projectId, 
			@PathVariable String groupId, 
			Model model,
			HttpSession session, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			mailMarketingService.deleteGroup(principal, groupId);
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/group";
	}
	
	@RequestMapping(value = "/{projectId}/mail/group/{groupId}/{recipientId}/delete", method = RequestMethod.GET)
	public String emailGroupRecipientDelete(
			@PathVariable Long projectId, 
			@PathVariable String groupId, 
			@PathVariable String recipientId, 
			Model model,
			HttpSession session, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			mailMarketingService.deleteReceipient(principal, groupId, recipientId);;
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/group/" + groupId;
	}

	@RequestMapping(value = "/{projectId}/mail/group/{groupId}/add", method = RequestMethod.POST)
	public String emailGroupAdd(
			@PathVariable Long projectId, 
			@PathVariable String groupId,
			@ModelAttribute AddEmailRecipient form, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			mailMarketingService.addReceipient(principal, groupId, form);
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/group/" + groupId;
	}

	@RequestMapping(value = "/{projectId}/mail/marketing", method = RequestMethod.GET)
	public String campaign(
			@PathVariable String projectId, 
			@RequestParam(required=false) Integer page,
			@RequestParam(required=false) Integer size,
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			Project project = projectService.getById(projectId);
			List<EmailMessage> list = mailMarketingService.getBlastData(project, page, size);
			model.addAttribute("project", project);
			model.addAttribute("list", list);
			return "marketing-mail/mail-dashboard";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/marketing";
	}

	@RequestMapping(value = "/{projectId}/mail/marketing/compose", method = RequestMethod.GET)
	public String composeCampaign(
			@PathVariable String projectId, 
			Model model, 
			HttpSession session,
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			Project project = projectService.getById(projectId);
			List<EmailGroup> groups = mailMarketingService.getMailGroups(project);
			List<EmailSender> senders = mailMarketingService.getValidEmailSender(project);
			model.addAttribute("project", project);
			model.addAttribute("senders", senders);
			model.addAttribute("groups", groups);
			return "marketing-mail/mail-compose";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/marketing";
	}

	@RequestMapping(value = "/{projectId}/mail/marketing/send", method = RequestMethod.POST)
	public String sendCampaign(
			@PathVariable String projectId, 
			@ModelAttribute ComposeMailForm form, 
			Model model,
			HttpSession session, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			Project project = projectService.getById(projectId);
			mailMarketingService.saveMailBlast(principal, project, form);
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/marketing";
	}

	@RequestMapping(value = "/{projectId}/mail/marketing/report/{messageId}", method = RequestMethod.GET)
	public String campaignReport(
			@PathVariable String projectId, 
			@PathVariable String messageId, 
			Model model,
			HttpSession session, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			EmailMessage email = mailMarketingService.getBlastDetail(messageId);
			Project project = projectService.getById(projectId);
			
			model.addAttribute("project", project);
			model.addAttribute("data", email);
			
			
			return "marketing-mail/mail-report";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/marketing";
	}
	
	@RequestMapping(value = "/{projectId}/mail/marketing/delivered/{messageId}", method = RequestMethod.GET)
	public String deliveredDetail(
			@PathVariable String projectId, 
			@PathVariable String messageId, 
			Model model,
			HttpSession session, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			EmailMessage email = mailMarketingService.getBlastDetail(messageId);
			Project project = projectService.getById(projectId);
			
			model.addAttribute("project", project);
			model.addAttribute("data", email);
			
			
			return "marketing-mail/mail-delivered-detail";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/marketing";
	}
	
	@RequestMapping(value = "/{projectId}/mail/marketing/opened/{messageId}", method = RequestMethod.GET)
	public String openDetail(
			@PathVariable String projectId, 
			@PathVariable String messageId, 
			Model model,
			HttpSession session, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			EmailMessage email = mailMarketingService.getBlastDetail(messageId);
			Project project = projectService.getById(projectId);
			
			model.addAttribute("project", project);
			model.addAttribute("data", email);
			
			
			return "marketing-mail/mail-opened-detail";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/marketing";
	}
	
	@RequestMapping(value = "/{projectId}/mail/marketing/clicks/{messageId}", method = RequestMethod.GET)
	public String clickDetail(
			@PathVariable String projectId, 
			@PathVariable String messageId, 
			Model model,
			HttpSession session, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			EmailMessage email = mailMarketingService.getBlastDetail(messageId);
			Project project = projectService.getById(projectId);
			
			model.addAttribute("project", project);
			model.addAttribute("data", email);
			
			return "marketing-mail/mail-clicked-all";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/marketing";
	}
	
	@RequestMapping(value = "/{projectId}/mail/marketing/clicks/{messageId}/detail/{urlId}", method = RequestMethod.GET)
	public String memberClicked(
			@PathVariable String projectId, 
			@PathVariable String messageId, 
			@PathVariable String urlId, 
			Model model,
			HttpSession session, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			Project project = projectService.getById(projectId);
			EmailMessage email = mailMarketingService.getBlastDetail(messageId);
			String linkId = urlId;
			
			model.addAttribute("project", project);
			model.addAttribute("data", email);
			model.addAttribute("linkId", linkId);
			
			
			return "marketing-mail/mail-clicked-detail";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/marketing";
	}
	
	@RequestMapping(value = "/{projectId}/mail/marketing/unsubscribe/{messageId}", method = RequestMethod.GET)
	public String unsubscribeDetail(
			@PathVariable String projectId, 
			@PathVariable String messageId, 
			Model model,
			HttpSession session, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			EmailMessage email = mailMarketingService.getBlastDetail(messageId);
			Project project = projectService.getById(projectId);
			
			model.addAttribute("project", project);
			model.addAttribute("data", email);
			
			
			return "marketing-mail/mail-unsubscribed-detail";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/" + projectId + "/mail/marketing";
	}

}
