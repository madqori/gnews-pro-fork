package io.gnews.pro.web.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.base.Throwables;

import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.web.config.AppConfig;
import io.gnews.pro.web.request.ProjectForm;
import io.gnews.pro.web.service.ProjectService;
import io.gnews.pro.web.service.SiteService;

@Controller
@RequestMapping(value = "/project")
public class ProjectController {
	
	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SiteService siteService;

	@Autowired
	private ProjectService projectService;

	@RequestMapping(method = RequestMethod.GET)
	public String index(
			HttpSession session, 
			Principal principal, 
			Model model,
			@RequestParam(required = false) String type,
			@RequestParam(required = false) String filter) {
		
		if (principal == null) {
			return "redirect:/";
		}
		siteService.setSiteInfo(principal, model);
		List<Project> projects = projectService.getUserProjects(principal, filter);
		
		model.addAttribute("list", projects);
		if(type == null){
			if(session.getAttribute(AppConfig.SESSION_PROJECT_TYPE) != null){
				type = session.getAttribute(AppConfig.SESSION_PROJECT_TYPE).toString();
			}else{				
				type = AppConfig.PROJECT_TYPE_GRID;
			}
		}
		if(type.equals(AppConfig.PROJECT_TYPE_LIST)){
			session.setAttribute(AppConfig.SESSION_PROJECT_TYPE, type);
			return "user/project-list";
		}else{
			session.removeAttribute(AppConfig.SESSION_PROJECT_TYPE);
			return "user/project-grid";
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/delete")
	public String delete(
			Principal principal, 
			Model model,
			@RequestParam String id) {
		if (principal == null) {
			return "redirect:/";
		}
		try{
			projectService.deleteProject(id);
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));	
		}
		return "redirect:/project";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save")
	public String add(
			HttpSession session, 
			Principal principal, 
			@ModelAttribute ProjectForm form) {
		if (principal == null) {
			return "redirect:/";
		}
		try{
			session.removeAttribute(AppConfig.SESSION_DATA_PAYMENT_ID);
			session.removeAttribute(AppConfig.SESSION_DATA_PROJECT_ID);
			projectService.createProject(principal, form, session);
			return "redirect:/payment/order";
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
			return "redirect:/project";
		}
	}
	
}
