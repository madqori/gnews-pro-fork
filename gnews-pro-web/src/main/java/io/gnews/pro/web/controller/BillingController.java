package io.gnews.pro.web.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.gnews.pro.core.model.mongodb.Payment;
import io.gnews.pro.web.config.AppConfig;
import io.gnews.pro.web.service.PaymentService;
import io.gnews.pro.web.service.SiteService;

@Controller
@RequestMapping("/billing")
public class BillingController {

	Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private SiteService siteService;

	@Autowired
	private PaymentService paymentService;

	@RequestMapping(method = RequestMethod.GET)
	private String billingHandler(HttpSession session, Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/";
		}
		siteService.setSiteInfo(principal, model);
		List<Payment> payments = paymentService.getPaymentData(principal, null);
		model.addAttribute("payments", payments);
		
		return "user/billing";
	}
	
	@RequestMapping(method = RequestMethod.GET, value ="/{id}")
	private String edit(
			HttpSession session, 
			Principal principal, 
			Model model, 
			@PathVariable Long id){
		if (principal == null){
			return "redirect:/";
		}
		session.setAttribute(AppConfig.SESSION_DATA_PROJECT_ID, id);
		return "redirect:/payment/order/";			
	}

}
