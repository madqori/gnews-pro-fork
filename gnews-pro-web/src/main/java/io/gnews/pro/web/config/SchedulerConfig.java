package io.gnews.pro.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author masasdani
 * Created Date Oct 27, 2015
 */
@Configuration
@EnableScheduling
public class SchedulerConfig {

}
