package io.gnews.pro.web.response;

import java.util.Date;
import java.util.List;

import io.gnews.pro.core.model.dto.TermCounter;

public class KeywordMappingData {
	
	private Long id;
	private Long idProject;
	private Date date;
	private List<TermCounter> termCounter;
	
	public KeywordMappingData() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getIdProject() {
		return idProject;
	}

	public void setIdProject(Long idProject) {
		this.idProject = idProject;
	}

	public List<TermCounter> getTermCounter() {
		return termCounter;
	}

	public void setTermCounter(List<TermCounter> termCounter) {
		this.termCounter = termCounter;
	}
	
}
