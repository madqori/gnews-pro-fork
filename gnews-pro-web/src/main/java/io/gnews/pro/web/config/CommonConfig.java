package io.gnews.pro.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.gnews.pro.web.util.RandomString;
import io.gnews.pro.web.util.RandomStringUppercase;

/**
 * config class for other bean
 * 
 * @author masasdani
 * Created Date Oct 27, 2015
 */
@Configuration
public class CommonConfig {

	@Bean
	public RandomString randomString() {
		return new RandomString(6);
	}

	@Bean
	public RandomStringUppercase randomStringUppercase() {
		return new RandomStringUppercase(6);
	}

}
