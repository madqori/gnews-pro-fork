package io.gnews.pro.web.service.impl;

import java.net.URL;
import java.util.Date;

import org.joda.time.DateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.model.mongodb.WebData;
import io.gnews.pro.core.repository.mongodb.ProjectRepository;
import io.gnews.pro.core.util.DomainUtil;
import io.gnews.pro.core.util.URLCleanerUtil;
import io.gnews.pro.web.config.AppConfig;
import io.gnews.pro.web.repository.mongodb.WebDataAnalyticRepository;
import io.gnews.pro.web.service.LanguageDetectorService;
import io.gnews.pro.web.service.TwitterMediaService;
import io.gnews.wispy.WebPage;
import io.gnews.wispy.Wispy;


/**
 * class to implement saving media / article
 * 
 * @author masasdani
 * Created Date Oct 27, 2015
 */
@Service
public class TwitterMediaServiceImpl implements TwitterMediaService{

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	WebDataAnalyticRepository analyticRepository;
	
	public static final int MIN_TITLE_LENGHT = 15;
	public static final int MIN_CONTENT_LENGHT = 400;
	public static final int MAX_SNIPPED_LENGHT = 200;
	public static final int MAX_CONNECTION_TIMEOUT = 30000;
	public static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 4.4; Nexus 5 Build/_BuildID_) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36";

	@Autowired
	Wispy wispy;
	
	@Autowired
	LanguageDetectorService languageDetectorService;
	
	@Autowired
	ProjectRepository projectRepository;

	/**
	 * @param id
	 * @param link
	 * @param date
	 */
	public void getWebdata(String id, String link, Date date) {
		
			Project project = projectRepository.findOne(id);
			if(analyticRepository.getWebData(project, link) != null){
				return;
			}
			Document document;
			if (!link.startsWith("http://") && !link.startsWith("https://")) {
				link = "http://" + link;
			}
			try {
				document = Jsoup.connect(link).userAgent(USER_AGENT)
						.timeout(MAX_CONNECTION_TIMEOUT).get();
				String extractedLink = document.baseUri();
				extractedLink = URLCleanerUtil.getCleanUrl(extractedLink);
				URL url = new URL(extractedLink);
				String domain = DomainUtil.getTopPrivateDomain(url.getHost());
				if(domain == null){
					return;
				}
				
				WebPage webPage = wispy.extractWebPage(url.toString(), document);
				String title = webPage.getTitle();
				String name = url.getHost();
				name = name.trim().toLowerCase();
				WebData data = new WebData();
				/*filtering empy content*/
				String content = webPage.getContent();
				if (content.length() < MIN_CONTENT_LENGHT) {
					return;
				}
				String language = languageDetectorService
						.getLanguage(content);
				if (!isLanguageSupported(language)) {
					log.error("lang : " + language);
					return;
				}
				/**
				 * validate title with length of words < 3 
				 */
				if(title.split("[\\p{Punct}\\s]+").length < 3){
					log.error("title : " + language);
					return;
				}
				DateTime dateTime = new DateTime(date);
				data = new WebData();
				data.setPeakDay(dateTime.dayOfMonth().roundFloorCopy().toDate().getTime());
				data.setPeakHour(dateTime.hourOfDay().roundFloorCopy().toDate().getTime());
				data.setUrl(url.toString());
				data.setHost(url.getHost());
				data.setDomain(domain);
				data.setTitle(title);
				data.setName(name);
				data.setLang(language);
				data.setContent(content);
				data.setDate(date);
				data.setFavicon(webPage.getFaviconUrl());
				String snipped = webPage.getDescription();
				if (snipped.length() <= title.length()) {
					snipped = content.substring(0, MAX_SNIPPED_LENGHT);
				} else if (snipped.length() > MAX_SNIPPED_LENGHT) {
					snipped = snipped.substring(0, MAX_SNIPPED_LENGHT);
				}
				snipped = snipped.replaceAll(" [^ ]+$", "");
				data.setSnippet(snipped + " ...");
				data.setImage(webPage.getMainImageUrl());
				if(data.getContent().length() > 0 || data.getSnippet().length() > 0){
				analyticRepository.saveWebData(project, data);
				}
				log.info("saving : " + data.getUrl() + "\n" + name);
				return;
				
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}

	/**
	 * @param languageCode
	 * @return
	 */
	public boolean isLanguageSupported(String languageCode){
		if(languageCode.equals(AppConfig.COUNTRY) || languageCode.equals(AppConfig.COUNTRY[0]) || 
				languageCode.equals(AppConfig.COUNTRY[1])){
			return languageCode.length()> 0;
		}
		return false;
	}
}
