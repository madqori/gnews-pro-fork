package io.gnews.pro.web.controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.base.Throwables;

import io.gnews.pro.core.util.DateUtils;
import io.gnews.pro.web.config.AppConfig;
import io.gnews.pro.web.service.ProjectService;
import io.gnews.pro.web.service.SiteService;

@Controller
@RequestMapping(value = "/project")
public class DashboardController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@Autowired
	private SiteService siteService;

	@Autowired
	private ProjectService projectService;
	
	@RequestMapping(value = "/{id}")
	public String showDashboard(@PathVariable Long id){
		return "redirect:/project/"+id+"/dashboard/keyword-mapping";
	}

	@RequestMapping(value = "/{id}/dashboard/keyword-mapping")
	public String keyMap(
			@PathVariable String id, 
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Device device,
			Principal principal) {
		try{
			if(end == null){
				end = new Date();
			}
			if(start == null){
				start = DateUtils.before(end, AppConfig.DEFAULT_ANALYTIC_DAY);
			}
			
			model.addAttribute("start", dateFormat.format(start));
			model.addAttribute("end", dateFormat.format(end));
			model.addAttribute("project", projectService.getById(id));
			session.setAttribute(AppConfig.SESSION_PROJECT_TYPE, id);
			siteService.setSiteInfo(principal, model);
			
			if (device.isMobile()) {
				return "analytics/mobile/keyword-mapping";
			} else {
				return "analytics/dashboard/keyword-mapping";
			}
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}
	
	@RequestMapping(value = "/{id}/media-analysis/keyword-mapping")
	public String keymoapbaru(
			@PathVariable Long id, 
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Device device,
			Principal principal) {
		try{
			if(end == null){
				end = new Date();
			}
			if(start == null){
				start = DateUtils.before(end, AppConfig.DEFAULT_ANALYTIC_DAY);
			}
			
			model.addAttribute("start", dateFormat.format(start));
			model.addAttribute("end", dateFormat.format(end));
			model.addAttribute("project", projectService.getById(id));
			session.setAttribute(AppConfig.SESSION_PROJECT_TYPE, id);
			siteService.setSiteInfo(principal, model);
			
			if (device.isMobile()) {
				return "media-analysis/ma-keyword-mapping";
			} else {
				return "media-analysis/ma-keyword-mapping";
			}
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}

	@RequestMapping(value = "/{id}/dashboard/peaktime")
	public String peakTime(
			@PathVariable String id, 
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Device device,
			Principal principal) {
		if(principal == null){
			return "redirect:/";
		}
		if(end == null){
			end = new Date();
		}
		if(start == null){
			start = DateUtils.before(end, AppConfig.DEFAULT_ANALYTIC_DAY);
		}
		
		model.addAttribute("start", dateFormat.format(start));
		model.addAttribute("end", dateFormat.format(end));
		model.addAttribute("project", projectService.getById(id));
		siteService.setSiteInfo(principal, model);
		if (device.isMobile()) {
			return "analytics/mobile/periodic-time";
		} else {
			return "analytics/dashboard/periodic-time";
		}
	}

	@RequestMapping(value = "/{id}/dashboard/top-media")
	public String top10(
			@PathVariable String id, 
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Device device,
			Principal principal) {

		if(principal == null){
			return "redirect:/";
		}
		if(end == null){
			end = new Date();
		}
		if(start == null){
			start = DateUtils.before(end, AppConfig.DEFAULT_ANALYTIC_DAY);
		}
		
		model.addAttribute("start", dateFormat.format(start));
		model.addAttribute("end", dateFormat.format(end));
		model.addAttribute("project", projectService.getById(id));
		siteService.setSiteInfo(principal, model);
		
		if (device.isMobile()) {
			return "analytics/mobile/top-media";
		} else {
			return "analytics/dashboard/top-media";
		}
	}

	@RequestMapping(value = "/{id}/dashboard/sentiment")
	public String sentiment(
			@PathVariable String id, 
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Device device,
			Principal principal) {

		if(principal == null){
			return "redirect:/";
		}
		if(end == null){
			end = new Date();
		}
		if(start == null){
			start = DateUtils.before(end, AppConfig.DEFAULT_ANALYTIC_DAY);
		}
		model.addAttribute("start", dateFormat.format(start));
		model.addAttribute("end", dateFormat.format(end));
		model.addAttribute("project", projectService.getById(id));
		siteService.setSiteInfo(principal, model);
		
		if (device.isMobile()) {
			return "analytics/mobile/sentiment";
		} else {
			return "analytics/dashboard/sentiment";
		}
	}

	@RequestMapping(value = "/{id}/dashboard/media-mapping")
	public String mediaMap(
			@PathVariable String id, 
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Device device,
			Principal principal) {

		if(principal == null){
			return "redirect:/";
		}
		if(end == null){
			end = new Date();
		}
		if(start == null){
			start = DateUtils.before(end, AppConfig.DEFAULT_ANALYTIC_DAY);
		}
		model.addAttribute("start", dateFormat.format(start));
		model.addAttribute("end", dateFormat.format(end));
		model.addAttribute("project", projectService.getById(id));
		siteService.setSiteInfo(principal, model);
		
		if (device.isMobile()) {
			return "analytics/mobile/media-mapping";
		} else {
			return "analytics/dashboard/media-mapping";
		}
	}
	
	@RequestMapping(value = "/{id}/dashboard/media-map")
	public String mediaMaping(
			@PathVariable String id, 
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Device device,
			Principal principal) {

		if(principal == null){
			return "redirect:/";
		}
		if(end == null){
			end = new Date();
		}
		if(start == null){
			start = DateUtils.before(end, AppConfig.DEFAULT_ANALYTIC_DAY);
		}
		model.addAttribute("start", dateFormat.format(start));
		model.addAttribute("end", dateFormat.format(end));
		model.addAttribute("project", projectService.getById(id));
		siteService.setSiteInfo(principal, model);
		
		if (device.isMobile()) {
			return "analytics/mobile/media-mapping";
		} else {
			return "analytics/media-mapping-chart";
		}
	}

	@RequestMapping(value = "/{id}/calendar")
	public String calendar(
			@PathVariable String id, 
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Principal principal) {

		if(end == null){
			end = new Date();
		}
		if(start == null){
			start = DateUtils.before(end, AppConfig.DEFAULT_ANALYTIC_DAY);
		}
		
		model.addAttribute("start", dateFormat.format(start));
		model.addAttribute("end", dateFormat.format(end));
		model.addAttribute("project", projectService.getById(id));
		siteService.setSiteInfo(principal, model);
		
		return "analytics/calendar";
	}
	
}
