package io.gnews.pro.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.repository.mongodb.AccountRepository;
import io.gnews.pro.web.service.AccountService;


@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository accountRepository;
	
	@Override
	public Account getAccountByToken(String accessToken) {
		return null; 
	}

	@Override
	public Account getAccountByEmail(String email) {
		return accountRepository.findByEmail(email);
	}

}
