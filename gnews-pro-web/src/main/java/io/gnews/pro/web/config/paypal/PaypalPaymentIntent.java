package io.gnews.pro.web.config.paypal;

public enum PaypalPaymentIntent {

	sale, authorize, order
	
}
