package io.gnews.pro.web.util;

import java.util.regex.Pattern;

public class RegexUtil {

	public static final String IMAGE_URL_REGEX = "(http(s?):/)(/[^/]+)+" + "\\.(?:jpg|png)";
	public static final String NON_ALPHA_NUMERIC_REGEX = "[^a-zA-Z0-9]";
	public static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public static final String FULLNAME_PATTERN = "^[\\p{L} .'-]+$";
	public static final String FULLNAME_PATTERN2 = "^[A-Za-z]+\\.([A-Za-z]\\.)?[A-Za-z]+$";
	
	public static boolean patternMatcher(String input, String regex){
		return Pattern.compile(regex).matcher(input).find();
	}
	
	public static String removeMatchRegex(String input, String regex){
		return input.replaceAll(regex, "");
	}
	
}
