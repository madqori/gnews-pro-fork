package io.gnews.pro.web.service.impl;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.config.RabbitMQConfig;
import io.gnews.pro.core.model.dto.MailType;
import io.gnews.pro.core.model.dto.SendEmailForm;
import io.gnews.pro.core.model.mongodb.GnewsLetter;
import io.gnews.pro.core.model.mongodb.Payment;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.model.mongodb.PromoCode;
import io.gnews.pro.core.model.mongodb.UserAccount;
import io.gnews.pro.core.repository.mongodb.GnewsLetterRepository;
import io.gnews.pro.core.repository.mongodb.PaymentRepository;
import io.gnews.pro.core.repository.mongodb.PromoCodeRepository;
import io.gnews.pro.core.repository.mongodb.UserAccountRepository;
import io.gnews.pro.web.config.AppConfig;
import io.gnews.pro.web.request.PaymentMethodForm;
import io.gnews.pro.web.request.PaymentOrderForm;
import io.gnews.pro.web.response.PromoCodeData;
import io.gnews.pro.web.service.PaymentService;
import io.gnews.pro.web.service.ProjectService;
import io.gnews.pro.web.service.UserService;
import io.gnews.pro.web.util.RandomStringUppercase;

@Service
public class PaymentServiceImpl implements PaymentService {

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private PaymentRepository paymentRepository;
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private PromoCodeRepository promoCodeRepository;

	@Autowired
	private RandomStringUppercase randomStringUppercase;
	
	@Autowired
	private UserAccountRepository userAccountRepository;

	@Autowired
	private UserService userService;
	
	@Autowired
	private GnewsLetterRepository gnewsLetterRepository;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	private String generateInvoice(){
		String invoice = randomStringUppercase.nextString();
		if(paymentRepository.findByInvoice(invoice) == null){
			return invoice;
		}else{
			return generateInvoice();
		}
	}

	@Override
	public Payment getByid(String id) {
		if(id == null) return null;
		return paymentRepository.findOne(id);
	}
	
	@Override
	public Payment createRecurrentProjectInvoice(Project project) {
		Payment payment = new Payment();
		payment.setProject(project);
		payment.setPeriod(1);
		payment.setPrice(AppConfig.DEFAULT_PRICE);
		payment.setTotal(payment.getPeriod() * payment.getPrice());
		payment.setInvoice(generateInvoice());
		payment.setCreatedAt(new Date());
		payment = paymentRepository.save(payment);
		return payment;
	}
	
	@Override
	public Payment savePaymentOrder(Principal principal, HttpSession session, PaymentOrderForm form) {		
		Payment payment =  getByid(form.getPaymentId());
		if(payment == null){
			payment = new Payment();
		}
		Project project = payment.getProject();
		if(project == null){
			project = projectService.getById(form.getProjectId());
			payment.setProject(project);
			session.setAttribute(AppConfig.SESSION_DATA_PROJECT_ID, project.getId());
		}
		payment.setPrice(AppConfig.DEFAULT_PRICE);
		PromoCode promoCode = promoCodeRepository.findByCodeAndSubmitedFalse(form.getCode());
		if(promoCode != null){
			payment.setPromoCode(promoCode);
			payment.setPrice(promoCode.getPrice());
			payment.setPeriod(promoCode.getPeriod());
			payment.getPromoCode().setSubmited(true);
		}else{
			if(project.getUser().getAgent() != null){
				payment.setPrice(project.getUser().getAgent().getDefaultPrice());
			}
			payment.setPeriod(form.getPeriod());
		}
		
		payment.setTotal(payment.getPeriod() * payment.getPrice());
		payment.setInvoice(generateInvoice());
		payment.setCreatedAt(new Date());		
		payment = paymentRepository.save(payment);
		session.setAttribute(AppConfig.SESSION_DATA_PAYMENT_ID, payment.getId());
		
		if(payment.getTotal() == 0){
			projectService.activate(project.getId(), payment.getPeriod(), true);
			//Activate User GnewsLetter
			List<GnewsLetter> gnewsLetters = gnewsLetterRepository.findByProjectId(project.getId());
			List<GnewsLetter> list = new ArrayList<>();
			for(GnewsLetter gnewsLetter : gnewsLetters){
				gnewsLetter.setActive(true);
			}
			gnewsLetterRepository.save(list);
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("invoiceDate", payment.getCreatedAt());
		log.info(payment.getCreatedAt() + "");
		map.put("name", project.getUser().getFullname());
		map.put("transferVia", payment.getMethod());
		map.put("invoice", payment.getInvoice());
		map.put("total", payment.getTotal());
		map.put("projectName", payment.getProject().getName());
		map.put("keyword", payment.getProject().getKeyword());
		map.put("filter", payment.getProject().getFilter());
		map.put("languages", payment.getProject().getLanguanges());
		map.put("period", payment.getPeriod());
		
		SendEmailForm emailForm = new SendEmailForm();
		emailForm.setType(MailType.INVOICE);
		emailForm.setTo(payment.getProject().getUser().getEmail());
		emailForm.setData(map);
		rabbitTemplate.convertAndSend(RabbitMQConfig.SEND_MAIL_QUEUE, emailForm);

		return payment;
	}

	@Override
	public Payment savePaymentMethod(Principal principal, HttpSession session, PaymentMethodForm form) {
		Payment payment = getByid(form.getPaymentId());
		UserAccount userAccount = userAccountRepository.findByEmail(principal.getName());
		payment.setMethod(form.getMethod());
		paymentRepository.save(payment);

		/* send email */
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("invoiceDate", payment.getCreatedAt());
		map.put("name", userAccount.getFullname());
		map.put("transferVia", payment.getMethod());
		map.put("invoice", payment.getInvoice());
		map.put("total", payment.getTotal());
		map.put("projectName", payment.getProject().getName());
		map.put("keyword", payment.getProject().getKeyword());
		map.put("filter", payment.getProject().getFilter());
		map.put("language", payment.getProject().getFilter());
		map.put("period", payment.getPeriod());
		
		SendEmailForm emailForm = new SendEmailForm();
		emailForm.setType(MailType.INVOICE);
		emailForm.setTo(payment.getProject().getUser().getEmail());
		emailForm.setData(map);
		rabbitTemplate.convertAndSend(RabbitMQConfig.SEND_MAIL_QUEUE, emailForm);
		
		session.removeAttribute(AppConfig.SESSION_DATA_PAYMENT_ID);
		session.removeAttribute(AppConfig.SESSION_DATA_PROJECT_ID);
		return payment;
	}

	@Override
	public Payment getByInvoice(String invoice) {
		return paymentRepository.findByInvoice(invoice);
	}
	
	@Override
	public PromoCodeData checkPromocode(HttpSession session, String code) {
		PromoCode promoCode = promoCodeRepository.findByCodeAndSubmitedFalse(code);
		if(promoCode == null){
			return null;
		}else{
			PromoCodeData codeData = new PromoCodeData();
			codeData.setPromocode(code);
			codeData.setPrice(promoCode.getPrice());
			codeData.setPeriod(promoCode.getPeriod());
			
			return codeData;
		}
	}

	@Override
	public List<Payment> getPaymentData(Principal principal, String type) {
		UserAccount user = userService.findUserByEmail(principal.getName());
		return paymentRepository.findByProjectUserAndProjectDeletedFalse(user);
	}

	@Override
	public Payment getPaymentById(String id) {
		return paymentRepository.findById(id);
	}

	@Override
	public Payment getProjectById(String id) {
		return paymentRepository.findProjectById(id);
	}

	@Override
	public void setPaid(Payment payment) {
		payment.setPaid(true);
		paymentRepository.save(payment);
		boolean newProject = !payment.getProject().isActive();
		projectService.activate(payment.getProject().getId(), payment.getPeriod(), newProject);
	}	
}
