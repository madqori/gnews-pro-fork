package io.gnews.pro.web.config;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @author masasdani
 * Created Date Oct 27, 2015
 */
@Configuration
@EnableAsync
public class AsyncConfig implements AsyncConfigurer {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(50000);
		executor.setMaxPoolSize(100000);
		executor.setQueueCapacity(100000);
		executor.setThreadNamePrefix("gnews-analytic-executor-");
		executor.initialize();
		return executor;
	}

	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return new CustomAsyncExceptionHandler();
	}

	class CustomAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

		@Override
		public void handleUncaughtException(Throwable throwable, Method method,
				Object... obj) {
			String error = "Exception message - " + throwable.getMessage()
					+ " Method name - " + method.getName();
			for (Object param : obj) {
				error += (" Parameter value - " + param);
			}
			log.error(error);

		}
	}
}
