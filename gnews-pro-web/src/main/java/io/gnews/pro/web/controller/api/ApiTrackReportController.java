package io.gnews.pro.web.controller.api;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.gnews.link.tracker.client.LinkTrackerClient;
import io.gnews.link.tracker.client.exception.RestException;
import io.gnews.link.tracker.client.request.GroupStatForm;
import io.gnews.link.tracker.client.request.LinkStatForm;
import io.gnews.link.tracker.client.request.SimplePaginatedForm;
import io.gnews.link.tracker.client.response.GroupData;
import io.gnews.link.tracker.client.response.LinkData;
import io.gnews.link.tracker.client.response.PeakCounter;
import io.gnews.link.tracker.client.response.TermCounter2;
import io.gnews.pro.core.model.mongodb.EmailRecipient;
import io.gnews.pro.core.repository.mongodb.EmailReceipientRepository;
import io.gnews.pro.web.response.EmailData;
import io.swagger.annotations.Api;

@Api(value="Mail Tracking Result", tags="Mail Tracking Result API")
@RestController
@RequestMapping("/api/track")
public class ApiTrackReportController {
	
	@Autowired
	private LinkTrackerClient linkTrackerClient;
	
	@Autowired
	private EmailReceipientRepository emailReceipientRepository;
	
	@RequestMapping(method = RequestMethod.GET, value = "/group")
	public List<GroupData> getGroupData(Principal principal,
			@ModelAttribute SimplePaginatedForm form) throws RestException {
		if(principal == null) return null;
		return linkTrackerClient.getGroupData(form);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/link")
	public List<LinkData> getLinkData(Principal principal,
			@ModelAttribute GroupStatForm form) throws RestException{
		if(principal == null) return null;
		return linkTrackerClient.getLinkData(form);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/group/visit")
	public Long getVisitCount(Principal principal,
			@ModelAttribute GroupStatForm form) throws RestException {
		if(principal == null) return null;
		return linkTrackerClient.getGroupVisitor(form);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/link/visit")
	public Long getVisitCount(Principal principal,
			@ModelAttribute LinkStatForm form) throws RestException {
		if(principal == null) return null;
		return linkTrackerClient.getLinkVisitor(form);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/group/visit/unique")
	public Long getVisitorVisitCount(Principal principal,
			@ModelAttribute GroupStatForm form) throws RestException {
		if(principal == null) return null;
		return linkTrackerClient.getVisitorVisitCount(form);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/link/visit/unique")
	public Long getVisitorVisitCount(Principal principal,
			@ModelAttribute LinkStatForm form) throws RestException {
		if(principal == null) return null;
		return linkTrackerClient.getLinkVisitorVisitor(form);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/group/click")
	public Long getVisitorClickCount(Principal principal,
			@ModelAttribute GroupStatForm form) throws RestException {
		if(principal == null) return null;
		return linkTrackerClient.getClickCount(form);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/group/click/unique")
	public Long getVisitorClickUniqueCount(Principal principal,
			@ModelAttribute GroupStatForm form) throws RestException {
		if(principal == null) return null;
		return linkTrackerClient.getVisitorClickCount(form);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/group/member")
	public Long getMemberCount(Principal principal,
			@ModelAttribute GroupStatForm form) throws RestException {
		if(principal == null) return null;
		return linkTrackerClient.getMemberCount(form);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/link/visitor")
	public List<EmailData> getVisitors(Principal principal,
			@ModelAttribute LinkStatForm form) throws RestException {
		if(principal == null) return null;
		List<EmailData> list = new ArrayList<>();
		List<String> emails = linkTrackerClient.getVisitors(form);
		List<EmailRecipient> recipients = emailReceipientRepository.findByEmailIn(emails);
		for(EmailRecipient recipient : recipients){
			list.add(new EmailData(recipient));
		}
		return list;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/group/visitor")
	public List<EmailData> getVisitors(Principal principal,
			@ModelAttribute GroupStatForm form) throws RestException {
		if(principal == null) return null;
		List<EmailData> list = new ArrayList<>();
		List<String> emails = linkTrackerClient.getVisitors(form);
		List<EmailRecipient> recipients = emailReceipientRepository.findByEmailIn(emails);
		for(EmailRecipient recipient : recipients){
			list.add(new EmailData(recipient));
		}
		return list;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/group/device")
	public List<TermCounter2> getDevice(Principal principal,
			@ModelAttribute GroupStatForm form) throws RestException {
		if(principal == null) return null;
		return linkTrackerClient.getDevice(form);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/group/browser")
	public List<TermCounter2> getBrowser(Principal principal,
			@ModelAttribute GroupStatForm form) throws RestException {
		if(principal == null) return null;
		return linkTrackerClient.getBrowser(form);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/group/country")
	public List<TermCounter2> getCountry(Principal principal,
			@ModelAttribute GroupStatForm form) throws RestException {
		if(principal == null) return null;
		return linkTrackerClient.getCountry(form);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/group/os")
	public List<TermCounter2> getOperatingSystem(Principal principal,
			@ModelAttribute GroupStatForm form) throws RestException {
		if(principal == null) return null;
		return linkTrackerClient.getOperatingSystem(form);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/group/peak")
	public List<PeakCounter> getPeakTime(Principal principal,
			@ModelAttribute GroupStatForm form) throws RestException {
		if(principal == null) return null;
		return linkTrackerClient.getPeakTime(form);
	}
		
}
