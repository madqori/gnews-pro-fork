package io.gnews.pro.web.controller.api;

import java.security.Principal;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.gnews.pro.core.model.mongodb.PromoCode;
import io.gnews.pro.core.repository.mongodb.PromoCodeRepository;
import io.gnews.pro.web.config.strings.ErrorMessage;
import io.gnews.pro.web.response.DefaultResponse;
import io.gnews.pro.web.response.PromoCodeData;
import io.gnews.pro.web.response.ResponseData;
import io.gnews.pro.web.service.PaymentService;

@RestController
@RequestMapping(value = "/api/promocode")
public class ApiCodeController {
	Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private PaymentService paymentService;
	
	@Autowired
	private PromoCodeRepository promoCodeRepository;

	@RequestMapping(method = RequestMethod.GET, value = "/{promocode}")
	public Object checkPromoCode(HttpSession session,
			@PathVariable(value = "promocode") String promocode, 
			Principal principal) {
		if (principal == null) {
			return null;
		} 
		PromoCodeData promoCode = paymentService.checkPromocode(session, promocode);
		if (promocode != null) {
			ResponseData<PromoCodeData> responseData = new ResponseData<>();
			PromoCode codeChecker = promoCodeRepository.findByCodeAndSubmitedFalse(promocode);
			if(codeChecker != null){
				responseData.setData(promoCode);
				return responseData;
			}else{
				responseData.setStatus(ErrorMessage.FAILED);
				responseData.setMessage(ErrorMessage.NO_DATA_ERROR);
				return responseData;
			}			
		}
		return DefaultResponse.noDataError();
	}

}
