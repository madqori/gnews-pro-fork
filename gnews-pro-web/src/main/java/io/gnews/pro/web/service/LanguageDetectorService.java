package io.gnews.pro.web.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.optimaize.langdetect.DetectedLanguage;
import com.optimaize.langdetect.LanguageDetector;
import com.optimaize.langdetect.text.TextObject;
import com.optimaize.langdetect.text.TextObjectFactory;

/**
 * class to implement language detection 
 * 
 * @author masasdani
 * Created Date Oct 27, 2015
 */
@Service
public class LanguageDetectorService {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private TextObjectFactory textObjectFactory;

	@Autowired
	private LanguageDetector languageDetector;

	/**
	 * get language from text
	 * 
	 * @param text
	 * @return
	 */
	public String getLanguage(String text){
		try{
			TextObject textObject = textObjectFactory.forText(text);
			List<DetectedLanguage> lang = languageDetector.getProbabilities(textObject);
			return lang.get(0).getLocale().getLanguage();
		}catch(Exception exception){
			return null;
		}
	}

}
