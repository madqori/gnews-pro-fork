package io.gnews.pro.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.gnews.link.tracker.client.LinkTrackerClient;
import io.gnews.link.tracker.client.impl.DefaultLinkTrackerClient;

@Configuration
public class LinkTrackerConfig {

	@Value("${link.tracker.auth.token}")
	private String accessToken;
	
	@Bean
	public LinkTrackerClient linkTrackerClient(){
		return new DefaultLinkTrackerClient(accessToken);
	}
	
}
