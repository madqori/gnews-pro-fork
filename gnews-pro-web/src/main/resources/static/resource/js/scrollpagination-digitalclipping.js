(function($) {
		   
			$.fn.scrollPagination = function(options) {
					
				return this.each(function(){
					var settings = { 
							nop     : 10,
							offset  : 10,
							speed	: 100, 
							error   : 'No More Posts!',
							delay   : 500, 
							scroll  : true, 
							url 	: "/api/report/"
						}
						
						if(options) {
							$.extend(settings, options);
						}	
					
					$this = $(this);
					$settings = settings;
					var offset = $settings.offset;
					var url = $settings.url;
					var busy = false;
					
					function applyLayout() {
						$("#grid").gridalicious({ 
			    			  gutter: 15, 
			    			  width: 180, 
			    			  animate: true, 
			    			  animationOptions: { 
			    			  speed: 200, 
			    			  duration: 100
			    			  }, 
			    		});
				    }
						
					applyLayout();
					
					function setElement(json){     
						var title = '';
		       		 	var host = '';
		       		    var img = '';
		       		    var id = $('#project_id').text();
		       		    var name = $('#project_name').text();
		       		    host = json.host;
		       		    var favicon = '';
		       		    if(json.favicon == null){
		       		    	favicon = '/resource/images/img_media_default.png';
		       		    }else{
		       		    	favicon = json.favicon;
		       		    }
		       		    
		       		 	if(json.content.length >= 140){
		       			 	desc = json.content.substring(0,140)+'..';
		       		 	}else{
		       		 		desc = json.content;
		       		 	}
		       		    
			       		var elems =
			       		 	'<div id="news-card" class="card">'+
							'<div class="card-main">'+
								'<div class="card-action" style="height: 30px;line-height: 20px;width: auto; overflow: hidden;">'+
									'<div class="card-header-side pull-left" style="padding-top: 15px;padding-bottom: 15px; padding-right:5px;">'+
										'<div class="avatar" style="width: 17px;height: 17px;border-radius:0;background-color: transparent;">'+
											'<img id="media" alt="" src="'+favicon+'" />'+
										'</div>'+
									'</div>'+
									'<div class="card-inner media" style="margin: 3px 11px;">'+
										'<span style="font-size: 13px;color: #4a4a4a;">&nbsp;'+json.host+'</span>'+
									'</div>'+
								'</div>'+
								'<div class="card-img waves-attach waves-effect">'+
									'<a href="http://gnews.id/article/read?url='+json.url+'" target="_blank"><img alt="" src="'+json.image+'"/></a>'+
								'</div>'+
								'<div class="card-action" style="height: 30px;border:none;">'+
									'<div class="card-inner" style="margin: 3px 11px;">'+
										'<a href="http://gnews.id/article/read?url='+json.url+'" target="_blank"><span style="font-size: 11px;color: #b7b7b7;">'+moment.unix(json.date).format('MMM DD, YYYY HH:mm')+'</span></a>'+
									'</div>'+
								'</div>'+
								'<div class="card-inner">'+
									'<p style="margin: 8px 0;">'+
										'<strong>'+
											'<a href="http://gnews.id/article/read?url='+json.url+'" target="_blank">'+json.title+'</a>'+
										'</strong>'+
										'<br/><a style="font-size: 10px;color: #808080;padding-top:7px;line-height:17px;" href="http://gnews.id/article/read?url='+json.url+'" target="_blank">'+json.content+'</a>'+
									'</p>'+
								'</div>'+
								'<div class="card-action">'+
									'<ul class="nav nav-list nav-justified" style="margin: 5px 0px;">'+
										'<li class="tooltip" title="edit news">'+
											'<a class="edit-news" name="'+json.url+'" type="'+json.title+'" img="'+json.image+'" ctn="'+json.content+'"  data-toggle="modal" href="#editNewsClipping">'+
											'<i class="fa fa-pencil-square-o" aria-hidden="true"></i>'+
											'</a>'+
										'</li>'+
										'<li class="tooltip" title="Delete News Clipping">'+
											'<a class="delete-news" name="/project/digital-clipping/delete?idProject='+ idProject +'&urlId='+json.url+'" data-toggle="modal" href="#modalDeleteClipping" style="cursor:pointer;">'+
												'<i class="fa fa-trash" aria-hidden="true"></i>'+
											'</a>'+
										'</li>'+
									'</ul>'+
								'</div>'+
							'</div>'+
						'</div>';
						
						return elems;
					}
					
					var idProject = $('#projectID').text();
					var accessToken = $('#access_token').text();
					var startDate = $('#startDates').text();
					var endDate = $('#endDates').text();
					function getData() {
						var content = [];
		                $.ajax({
							 type: "GET", 
							 url: url + idProject + "/peak-day/article?accessToken=" +  accessToken + "&start=" + startDate + "&end=" + endDate + "&limit=10" +  "&peakDay=" + currentDay + "&offset="+ offset,
							 beforeSend : function(){
							 },
					         success: function(json){
					        	 if(json.data.length >= 1){
					        		 $.each(json.data, function(){
						        		 content.push($(setElement(this)));
						        	 });			        	 			        	 
						        	 $this.gridalicious('append', content);
							         $this.find('img').error(function(){		
							        	 $(this).attr("src", "/resource/images/default-preffered.png");
							         });
							         offset = offset + $settings.nop;
					        	 }else {
					        		 return null;
					        	 }
						     },	
						     complete : function(i, json){	
						     	initClick();
						        busy = false;
						     },
						     failure: function(data) {
						     }					    		
				        });			                
		                                        
					}	
					
					// If scrolling is enabled
					if($settings.scroll == true) {
						$(window).scroll(function() {					
							if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {									
								busy = true;
								setTimeout(function() {
									getData();
									console.log(offset);
								}, $settings.delay);
							}	
						});
					}
					
				});
			}

		})(jQuery);