
		var projectName			= $('#project_name').text();
		var idProject 			= $('#project_id').text();
		var currentTerm			= '';
		var canvas 				= document.getElementById('keywords-mapping');
		
		var addrMediaCount 		= $('#apiReport').attr('href') + idProject + "/media-count?accessToken=" +  $('#access_token').text() + "&start=" + startDate + "&end=" + endDate;  
		var addrArticleCount 	= $('#apiReport').attr('href') + idProject + "/article-count?accessToken=" +  $('#access_token').text() + "&start=" + startDate + "&end=" + endDate;
		
		var addrKeywordMapping 	= $('#apiReport').attr('href') + idProject + "/keyword-mapping?accessToken=" +  $('#access_token').text() + "&start=" + startDate + "&end=" + endDate;
		var addrTermKeywordMapping = $('#apiReport').attr('href') + idProject + "/keyword-mapping/article?accessToken=" +  $('#access_token').text() + "&limit=10&start=" + startDate + "&end=" + endDate + "&term=";
		var addrTotalArticle 	= $('#apiReport').attr('href') + idProject + "/keyword-mapping/articlecount?accessToken=" +  $('#access_token').text() + "&start=" + startDate + "&end=" + endDate + "&term=";
		var addrTotalMedia 		= $('#apiReport').attr('href') + idProject + "/keyword-mapping/mediacount?accessToken=" +  $('#access_token').text() + "&start=" + startDate + "&end=" + endDate + "&term=";
		
		var addrMostMedia 		= $('#apiReport').attr('href') + idProject + "/most-media?accessToken=" +  $('#access_token').text() + "&start=" + startDate + "&end=" + endDate + "&limit=";
		var addrTermMediaMapping= $('#apiReport').attr('href') + idProject + "/most-media/article?accessToken=" +  $('#access_token').text() + "&start=" + startDate + "&end=" + endDate + "&limit=10&term=";
		
		var addrSentimentPos	= $('#apiReport').attr('href') + idProject + "/sentiment/daypositive?accessToken=" +  $('#access_token').text() + "&start=" + startDate + "&end=" + endDate;
		var addrSentimentNeg	= $('#apiReport').attr('href') + idProject + "/sentiment/daynegative?accessToken=" +  $('#access_token').text() + "&start=" + startDate + "&end=" + endDate;
		var addrSentimentNeu	= $('#apiReport').attr('href') + idProject + "/sentiment/dayneutral?accessToken=" +  $('#access_token').text() + "&start=" + startDate + "&end=" + endDate;
		var addrSentimentPeakDay= $('#apiReport').attr('href') + idProject + "/peak-day/article?accessToken=" +  $('#access_token').text() + "&start=" + startDate + "&end=" + endDate + "&peakDay=" + day;
		
		var addrPeakHourCount 	= $('#apiReport').attr('href') + idProject + "/peak-hour-media?accessToken=" +  $('#access_token').text();
		var addrPeakDay 		= $('#apiReport').attr('href') + idProject + "/peak-day?accessToken=" +  $('#access_token').text() + "&start=" + startDate + "&end=" + endDate;
		var addrPeakHour 		= $('#apiReport').attr('href') + idProject + "/peak-hour?accessToken=" +  $('#access_token').text() + "&start=" + startDate + "&end=" + endDate;
		var addrPeakHourById 	= $('#apiReport').attr('href') + idProject + "/peak-hour/article?accessToken=" +  $('#access_token').text() + "&limit=10&start=" + startDate + "&end=" + endDate + "&peakHour=" + day;
		
		function loadNews(type, term) {
			var content = [];
			var x = 0;
			if(type === "keywordmap"){
				var url =  addrTermKeywordMapping + term + "&offset="+ loadedArticles;
			}else if(type === "topmedia"){
				var url =  addrTermMediaMapping + term + "&offset="+ loadedArticles;
			}
			else if(type === "periodic"){
				var url = addrPeakHourById + term + "&offset="+ loadedArticles;
			}
			else {
				var url = addrSentimentPeakDay + timestamp + "&limit=10&start=" + "&sentiment="+ type + "&offset="+ loadedArticles;
			}
			
			onArticleLoading = true;
			$.ajax({
				type: "GET",
				url: url,
				data: '',
			 	success:  function(json){
			 		loadedArticles += json.data.length;
			 		var elems = '';
			 		$.each(json.data, function(){
			 			function setElement(json){
			 				var title = '';
			       		 	var host = '';
			       		    var img = '';
			       		    var favicon = '';
			       		    if(json.favicon == null){
			       		    	favicon = '/resource/images/img_media_default.png';
			       		    }else{
			       		    	favicon = json.favicon;
			       		    }
			       		    
			       		 	elems =
			       		 	'<div id="news-card" class="card">'+
							'<div class="card-main">'+
								'<div class="card-action" style="height: 30px;line-height: 20px;width: auto; overflow: hidden;">'+
									'<div class="card-header-side pull-left" style="padding-top: 15px;padding-bottom: 15px; padding-right:5px;">'+
										'<div class="avatar" style="width: 17px;height: 17px;border-radius:0;background-color: transparent;">'+
											'<img id="media" alt="" src="'+favicon+'" />'+
										'</div>'+
									'</div>'+
									'<div class="card-inner media" style="margin: 3px 11px;">'+
										'<span style="font-size: 13px;color: #4a4a4a;">&nbsp;'+json.host+'</span>'+
									'</div>'+
								'</div>'+
								'<div class="card-img waves-attach waves-effect">'+
									'<a href="http://gnews.id/article/read?url='+json.url+'" target="_blank"><img alt="" src="'+json.image+'"/></a>'+
								'</div>'+
								'<div class="card-action" style="height: 30px;border:none;">'+
									'<div class="card-inner" style="margin: 3px 11px;">'+
										'<a href="http://gnews.id/article/read?url='+json.url+'" target="_blank"><span style="font-size: 11px;color: #b7b7b7;">'+moment.unix(json.date).format('MMM DD, YYYY HH:mm')+'</span></a>'+
									'</div>'+
								'</div>'+
								'<div class="card-inner">'+
									'<p style="margin: 8px 0;">'+
										'<strong>'+
											'<a href="http://gnews.id/article/read?url='+json.url+'" target="_blank">'+json.title+'</a>'+
										'</strong>'+
										'<br/><a style="font-size: 10px;color: #808080;padding-top:7px;line-height:17px;" href="http://gnews.id/article/read?url='+json.url+'" target="_blank">'+json.content+'</a>'+
									'</p>'+
								'</div>'+
							'</div>'+
						'</div>';
						return elems;
			 			}
			 			content.push($(setElement(this)));
			 		 });
			 		 $('#grid').gridalicious('append', content);
			 		 $('#grid').find('img').error(function(){	
		        		 $(this).attr("src", "/resource/images/default-preffered.png");
			         });
			 		onArticleLoading = false;
			 		}
			 	});
		}
		
		function allTermsByDate(container_place){
			$(container_place).html(' ');
		  	var list = [];
		  	var count = 10;
			$.ajax({
				 type: "GET",
			     url: addrKeywordMapping,
			     data: '',		     				     				         
		         beforeSend: function(){
		         },
		         complete: function(){
		         },
		         success: function(data){
		        	 values = data.data[0];
		        	 var minp = data.data[data.data.length - 1].value;
		        	 var maxp = data.data[0].value;
		        	 $.each(data.data, function(){
		        		  	count = logScale(this.value, minp, maxp); 
		        		  	list.push([this.term, count]);
		         	 }); 
		        	 var $canvas = $(container_place);
		             var width = $(container_place).parent().width();
		             var height = '470px';            
		             $canvas.attr('width', width);
		             $canvas.attr('height', height);
		             if(list[0]){
		            	 currentTerm = values.term;
		            	 loadArticles(values.term);
		           	 }
		             var options = {
		                list : list,
		                gridSize: Math.round(12 * $(container_place).width() / width),
		                weightFactor: function (size) {		                  		
		                	return Math.pow(size, 0.9) * $(container_place).width() / width;
		                },
		                fontFamily: '"HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif',
		                color:function(size,i){
		                	return (size == values.term) ? '#00A0DC' : '#000';
		                } ,
		                fontWeight : 'bold',   
		                rotateRatio: 0,
		                shape : 'square',
		                backgroundColor: 'transparent',
		                hover: function(item, dimension){
		                	if(item){
		                		canvas.style.cursor='pointer';
		                	}else{
		                		canvas.style.cursor='default';		                		
		                	}
		                },
		                click: function(item) {
		                	item[0].fillStyle = '#00A0DC';
		                	currentTerm = item[0];
		                	loadArticles(item[0]);
		                  	stateLoadInput = 5;
		                  	$('html, body').animate({
		                  		scrollTop: $("#grid").offset().top - 80
		                    }, 500);
		                 },
		              };		             
		              WordCloud($(container_place)[0], options);
		              list = [];
		         },
		         complete : function(){	
			    	 $(".loader").fadeOut("slow");
			     }
			});  
		}
		
		function logScale(value, minp, maxp){
			var minv = Math.log(20);
			var maxv = Math.log(200);
			var scale = (maxv - minv) / (maxp-minp);
			return Math.exp(minv + scale * (value - minp));
		}
		
		function sentimentChart () {
			var positive = [];
			var neutral = [];
			var negative = [];
			var lastDay;
			var lastValue;
			$.ajax({
		    	type: "GET",
		    	url: addrSentimentPos,
		    	data: '',
		    	beforeSend: function() {
		    	},
		    	success: function(json){
		    		$.each(json.data, function(i, item){
		    			positive.push([item.peakDay, item.count]);
		    			lastDay = item.peakDay;
		    			lastValue = item.count;
		    		});
		    		$.ajax({
		    			type: "GET",
				    	url: addrSentimentNeg,
				    	data: '',
				    	beforeSend: function() {
				    	},
				    	success: function(json){
				    		$.each(json.data, function(i, item){
				    			negative.push([item.peakDay, item.count]);
				    		});
				    		$.ajax({
				    			type: "GET",
						    	url: addrSentimentNeu,
						    	data: '',
						    	beforeSend: function() {
						    	},
						    	success: function(json){
						    		$.each(json.data, function(i, item){
						    			neutral.push([item.peakDay, item.count]);
						    		});
						    		
						    		$('#sentiment-chart').highcharts({
								        title: {
								            text: ''
								        },
								        xAxis: {
								        	type: 'datetime',
								        	minTickInterval: 24 * 3600 * 1000
								        },
								        yAxis: {
								            min: 0,
								            title: {
								                text: 'Total news'
								            },
								            plotLines: [{
								                value: 0,
								                width: 1,
								                color: '#808080'
								            }]
								        },
								        tooltip: {
								        	valueSuffix: ' news'
								        },
								        legend: {
								            layout: 'horizontal',
								            align: 'center',
								            borderWidth: 0
								        },
								        credits: {
								            enabled: false
								        },
								        plotOptions: {
								            column: {
								                pointPadding: 0.05,
								                borderWidth: 0
								            }
								        },
								        series: [{
								            name: 'Positive',
								            color: '#00a0dc',
								            data: positive,
								            cursor: 'pointer',
					 		                point: {
					 		                    events: {
					 		                        click: function () {
					 		                        	currentSentiment = 'positive';
					 		                        	timestamp = this.x;
					 		                        	$(".currentTime").text(moment(this.x).format('dddd, DD MMM YYYY '));
					 		                        	$("#totalArticle").text(this.y);
					 		                        	totalArticle = this.y;
					 		                        	loadArticles(this.x, this.y);
					 		                       		$('html, body').animate({
						 		                            scrollTop: $("#grid").offset().top - 80
						 		                        }, 500);
					 		                         }
					 		                    }
					 		                }
								         },{
								        	 name: 'Neutral',
								        	 color: '#eaeaea',
									         data: neutral,
									         point: {
						 		                    events: {
						 		                        click: function () {
						 		                        	currentSentiment = 'neutral';
						 		                        	timestamp = this.x;
						 		                        	$(".currentTime").text(moment(this.x).format('dddd, DD MMM YYYY '));
						 		                        	$("#totalArticle").text(this.y);
						 		                        	loadArticles(this.x, this.y);
						 		                       		$('html, body').animate({
							 		                            scrollTop: $("#grid").offset().top - 80
							 		                        }, 500);
						 		                         }
						 		                    }
						 		                }
								        }, {
								        	name: 'Negative',
								            color: '#ff7575',
								            data: negative,
								            point: {
					 		                    events: {
					 		                        click: function () {
					 		                        	currentSentiment = 'negative';
					 		                        	timestamp = this.x;
					 		                        	$(".currentTime").text(moment(this.x).format('dddd, DD MMM YYYY '));
					 		                        	$("#totalArticle").text(this.y);
					 		                        	loadArticles(this.x, this.y);
					 		                       		$('html, body').animate({
						 		                            scrollTop: $("#grid").offset().top - 80
						 		                        }, 500);
					 		                         }
					 		                    }
					 		                }
								        }]
								    });
						    		if(lastDay){
						    			timestamp = lastDay;
						    			currentSentiment = 'positive';
						    			$(".currentTime").text(moment(lastDay).format('dddd, DD MMM YYYY '));
				                     	$("#totalArticle").text(lastValue);
						    			loadArticles(lastDay,lastValue);
						    		}
						    	}
				    		});
				    	}
		    		});
		    	
		    	}
		    });
		}
		
		function topmedia(limit) {
			$.ajax({
				 type: "GET",
			     url: addrMostMedia+limit,
			     data: '',		     				     				         
		         beforeSend: function() { 				        	 
		        	
		         },
		         complete: function() { 		        	 
		        	 
		         },
		         success: function(json){
		        	 var terms = [];
		        	 var n = 0;
		           	 $.each(json.data, function(i, item){
		             	terms.push([item.term]);
		             	n++;
		             });
		           	 var values = [];
		           	 $.each(json.data, function(i, item){
		             	values.push([item.value]);		             
		             });
		           	if(terms[0]){
		           		$("#totalArticle").text(json.data[0].value);
		           		$(".mediaName").text(json.data[0].term);
		           		loadArticles(json.data[0].term, json.data[0].value);
		           		currentTerm = json.data[0].term;
		              }
		        	 $('#topten-chart').highcharts({
		 		        chart: {
		 		            type: 'bar'
		 		        },
		 		       	credits: {
		 	            	enabled: false
		 	            },
		 		        title: {
		 		                text: ''
		 		            },
		 		        colors: ['#00A0DC'],
		 		        xAxis: {
		 		            categories: terms
		 		        },
		 		        tooltip: {
		 		            valueSuffix: ' millions'
		 		        },
		 		        legend: {
		 		            layout: 'vertical',
		 		            floating: true,
		 		            backgroundColor: '#FFFFFF',
		 		            align: 'right',
		 		            verticalAlign: 'bottom',
		 		            y: -32,
		 		            x: 10
		 		        },
		 		        tooltip: {
		 		            formatter: function () {
		 		                return '<b>' + this.series.name +'</b><br/>' +
		 		                    this.x + ': ' + this.y;
		 		            }
		 		        },
		 		       plotOptions: {
		 		    	  bar: {
		 		                dataLabels: {
		 		                    enabled: true
		 		                }
		 		            },
		 		            series: {
		 		                cursor: 'pointer',
		 		                point: {
		 		                    events: {
		 		                        click: function (item) {
		 		                        	$("#totalArticle").text(json.data[0].value);
		 		   		           			$(".mediaName").text(json.data[0].term);
		 		                        	loadArticles(terms[this.x], this.y);
		 		                        	currentTerm = terms[this.x];
		 		                        	$('html, body').animate({
			 		                            scrollTop: $("#grid").offset().top - 80
			 		                        }, 500);
		 		                         }
		 		                    }
		 		                }
		 		            }
		 		        },
		 		        series: [{
		 		            name: 'News',
		 		            data: values,
		 		            url: terms
		 		        }]
		 		    });
		       	 }
			});
		}
		
		function getPeaktime(container){
        	var peaktime = [];
        	var peakday = [];
	       	 $.ajax({
				 type: "GET",
			     url: addrPeakHour,
			     data: '',		     				     				         
		         beforeSend: function() {
		         },
		         success: function(data){
		        	 $.ajax({
						 type: "GET",
					     url: addrPeakHour,
					     data: '',		     				     				         						         
					     success: function(json){
					    	 var values = '';
					    	 var getFirst = 0;
					    	 var n = 0;
				    		$.each(json.data, function(i, item){
				    			peaktime.push([item.id, item.value]);
				    			n++;
						    });
				    		$.each(data.data, function(i, item){        		
				         		peakday.push([item.id, item.value]);
				         		getFirst++;
				         	});
				    		if(peakday[getFirst-1]){
				    			  values=peakday[getFirst-1];
				    			  currentPeak = values[0];
				    			  $(".currentTime").text(moment(values[0]).format('dddd, DD MMM YYYY - h:mm a'));
					 	   		  $("#totalArticle").text(values[1]);
				    			  loadPeakHour(values[0], values[1]);
				            	  
				              }
				         	peakday.sort();
				         	peaktime.sort();
				         	
				         	$(container).highcharts({
				         		
				         		title: {
				                    text: ''
				                },
				                chart: {
				                    zoomType: 'x'
				                },
				                credits: {
				                    enabled: false
				                   },


				                xAxis: {
				                    type: 'datetime',
				                    dateTimeLabelFormats: {
				                       day: '%a %d/%b',
				                       millisecond: '%H:%M:%S.%L',
				                       second: '%H:%M:%S',
				                       minute: '%H:%M',
				                       hour: '%H:%M',
				                       week: '%e %b',
				                       month: '%b \'%y',
				                       year: '%Y'
				                    }
				                },
				                yAxis: {
				                    title: {
				                        text: null
				                    }
				                },
				                tooltip: {
				                	formatter: function() {
			                               return moment(this.x).format('MMM DD, YYYY HH:mm') +'<br/><div class="articlebox"></div><b>' + this.y + '</b> news ';
			                           }
				                },
				                legend: {
				                	align: 'right',
			    	                verticalAlign: 'top',
			    	                y: 0,
			    	                x: -40,
			    	                floating: true,
			    	                borderWidth: 0
				                },
				                plotOptions: {
				                    series: {
				                        cursor: 'pointer',
				                        point: {
				                            events: {
				                                click: function (e) {
				                                	$(".currentTime").text(moment(this.x).format('dddd, DD MMM YYYY - h:mm a'));
				                                	$("#totalArticle").text(this.y);
				                                	currentPeak = this.x;
				                                	loadPeakHour(this.x,this.y);
				                                	$('html, body').animate({
					 		                            scrollTop: $("#grid").offset().top - 80
					 		                        }, 500);
				                                }
				                            }
				                        },
				                        marker: {
				                            lineWidth: 1
				                        }
				                    }
				                },

				                subtitle: {
				                    text: 'Click and drag to zoom in.'
				                },

				                series: [{
				                    name: 'Peak by Hours',
				                    data: _.unique(peaktime),
				                    zIndex: 1,
				                    marker: {
				                    	fillColor: 'white',
					                    lineWidth: 2,
					                    lineColor: Highcharts.getOptions().colors[0]
				                    }
				                }]
				             });
					     },
					     failure: function(data) {
					    	
					     }	    
					});
			     }
			});
        	
        }
		
		function loadPeakHour(term,count) {
			loadedArticles = 0;
			$.ajax({
	     	      type: "GET",
	 	          url: addrPeakHourCount + "&peakHour=" +term,                                
	 	          data: "",
	 	      	  dataType: "json",
	 	       	  cache: false,
	 	      	  beforeSend : function(){
	 	      	  },
	 	          success : function(data){
	 	          },complete : function(data){   	        	
	 	        	var obj = JSON.parse(data.responseText);
	 	        	totalMedia = $.number(obj.data);
	 	        	loadContainer();
	 	   			
	 	   			console.log(type);
	 	   			loadNews(type, currentPeak);
	 	          }
	     	   });	
		}
		
		function articleCountByterm(term){
        	$.ajax({
         	      type: "GET",
     	          url: addrTotalArticle + term,                                
     	          data: "",
     	      	  dataType: "json",
     	       	  cache: false,
     	      	  beforeSend : function(){
     	      	  },
     	          success : function(data){
     	          },complete : function(data){  
     	        	var obj = JSON.parse(data.responseText);
     	        	var value = $.number(obj.data);
  			    	$('#totalArticle').number(value);
  			    	$('#totalArticle').attr('value', value);
     	          }
         	   });
		}

		function mediaCountByTerm(term){
        	$.ajax({
         	      type: "GET",
     	          url: addrTotalMedia + term,   
     	          data: "",
     	      	  dataType: "json",
     	       	  cache: false,
     	      	  beforeSend : function(){
     	      	  },
     	          success : function(data){
     	          },complete : function(data){
     	        	var obj = JSON.parse(data.responseText);
     	        	var value = $.number(obj.data);
  			    	$('#totalMedia').number(value);
  			    	$('#totalMedia').attr('value', value);
     	          }
         	   });
		}
		
		function loadContainer (){
			loadedArticles = 0;
	   		$('#grid').html('');
	   		$("#grid").gridalicious({ 
	  			  gutter: 15, 
	  			  width: 180, 
	  			  animate: true, 
	  			  animationOptions: { 
	  			    speed: 100, 
	  			    duration: 100
	  			  }, 
	  			});
		}
		