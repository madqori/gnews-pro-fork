package io.gnews.pro.report.service.impl;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.DateTickUnitType;
import org.jfree.chart.axis.TickUnits;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.util.ShapeUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.kennycason.kumo.CollisionMode;
import com.kennycason.kumo.WordCloud;
import com.kennycason.kumo.WordFrequency;
import com.kennycason.kumo.bg.RectangleBackground;
import com.kennycason.kumo.font.KumoFont;
import com.kennycason.kumo.font.scale.LinearFontScalar;
import com.kennycason.kumo.image.AngleGenerator;
import com.kennycason.kumo.palette.ColorPalette;
import com.kennycason.kumo.wordstart.CenterWordStart;

import io.gnews.pro.core.model.dto.PeakCounter;
import io.gnews.pro.core.model.dto.SentimentPeakCounter;
import io.gnews.pro.core.model.dto.TermCounter;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.report.repository.WebDataReportRepository;
import io.gnews.pro.report.service.ReportService;

@Service
public class ChartGeneratorMonthly {

	@Value("${report.data.url}")
	private String urlGnews;
	
	@Value("${report.data.monthly}")
	private String monthlyDir;
	
	@Value("${report.data.doc}")
	private String docDir;
	
	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ReportService reportService;

	@Autowired
	private WebDataReportRepository analyticRepository;

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	public void generatePeakDay(String id, Date start, Date end, Integer offset, Integer limit, int i) throws IOException {
		Project project = reportService.getById(id);
		List<PeakCounter> frequencies = analyticRepository.getPeakDay(project, start, end);
		DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
		for (PeakCounter peakCounter : frequencies) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd");
			String string = simpleDateFormat.format(peakCounter.getId());
			Double term = (double) peakCounter.getValue();
			dataSet.addValue(term, "News", string);
		}

		JFreeChart lineChartObject = ChartFactory.createLineChart("", "", "", dataSet, PlotOrientation.VERTICAL, true,
				true, false);
		CategoryPlot cplot = (CategoryPlot) lineChartObject.getPlot();
		cplot.setBackgroundPaint(Color.white);
		cplot.setDomainGridlinePaint(Color.black);
		cplot.setRangeGridlinePaint(Color.white);
		cplot.setDomainGridlinesVisible(true);
		cplot.getRenderer().setSeriesStroke(0, new BasicStroke(2));
		cplot.getRenderer().setSeriesStroke(1, new BasicStroke(2));
		cplot.getRenderer().setSeriesStroke(2, new BasicStroke(2));
		cplot.getRenderer().setSeriesPaint(0, new Color(1686005));
		cplot.getRenderer().setSeriesPaint(1, new Color(15395562));
		cplot.getRenderer().setSeriesPaint(2, new Color(16748174));
		cplot.setDomainCrosshairVisible(true);
		cplot.setRangeCrosshairVisible(true);
		cplot.setOutlineVisible(false);
		lineChartObject.getLegend().setFrame(BlockBorder.NONE);
		final LineAndShapeRenderer renderer = (LineAndShapeRenderer) cplot.getRenderer();
		renderer.setSeriesPaint(1, Color.YELLOW);
		renderer.setSeriesPaint(2, Color.YELLOW);

		renderer.setBaseShapesFilled(true);
		renderer.setBaseShapesVisible(true);

		int width = 710;
		int height = 430;
		File lineChart = new File(monthlyDir+"peakDay"+ i + "_" + project.getId() + ".png");
		ChartUtilities.saveChartAsPNG(lineChart, lineChartObject, width, height);
		System.out.println("Generate PeakDay " + project.getName() + "Done");
	}

	public void generateTopMedia(String id, Date start, Date end, Integer offset, Integer limit, int i) throws IOException {
		Project project = reportService.getById(id);
		List<TermCounter> frequencies = analyticRepository.getMostMedia(project, start, end, limit, offset);
		DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
		for (TermCounter termCounter : frequencies) {
			Double term = (double) termCounter.getValue();
			dataSet.addValue(term, "News", termCounter.getTerm());
		}

		JFreeChart lineChartObjectTop = ChartFactory.createBarChart("", "", "", dataSet, PlotOrientation.HORIZONTAL, true,
				true, false);
		CategoryPlot cplot = (CategoryPlot) lineChartObjectTop.getPlot();
		// change background color
		cplot.setBackgroundPaint(new Color(16777215));
		cplot.setOutlineVisible(false);
		lineChartObjectTop.getLegend().setFrame(BlockBorder.NONE);
		// set bar chart color
		((BarRenderer) cplot.getRenderer()).setBarPainter(new StandardBarPainter());
		BarRenderer r = (BarRenderer) lineChartObjectTop.getCategoryPlot().getRenderer();
		r.setSeriesPaint(0, new Color(1686005));
		int width = 710;
		int height = 430;
		File topMediaChart = new File(monthlyDir+"topMedia" +  i + "_" + project.getId() + ".png");
		ChartUtilities.saveChartAsPNG(topMediaChart, lineChartObjectTop, width, height);
		System.out.println("Generate TopMedia " + project.getName() + "Done");
	}

	public void generateWordCloud(String id, Date start, Date end, int i){
		Project project = reportService.getById(id);
		/* change to get data from mongoDb */
		List<TermCounter> frequencies = analyticRepository.getKeywordMappingNew(project, start, end);
		final List<WordFrequency> wordFrequencies = new ArrayList<WordFrequency>();
		final Font[] FONTS = new Font[] { 
	            new Font("arial", Font.BOLD, 10)};
		for (TermCounter termCounter : frequencies) {
			WordFrequency wordFrequency = new WordFrequency(termCounter.getTerm(), termCounter.getValue());
			wordFrequencies.add(wordFrequency);
		}
		final Dimension dimension = new Dimension(710, 430);
		final WordCloud wordCloud = new WordCloud(dimension, CollisionMode.RECTANGLE);
		final Color backgroundColor = new Color(255, 255, 255);
		wordCloud.setPadding(1);
		wordCloud.setAngleGenerator(new AngleGenerator(0));
		wordCloud.setBackground(new RectangleBackground(dimension));
		wordCloud.setBackgroundColor(backgroundColor);
		wordCloud.setColorPalette(new ColorPalette(new Color(0,0,0)));
		wordCloud.setFontScalar(new LinearFontScalar(10, 40));
		wordCloud.setWordStartScheme(new CenterWordStart());
		wordCloud.setKumoFont(new KumoFont(FONTS[0]));
		wordCloud.build(wordFrequencies);
		wordCloud.writeToFile(monthlyDir+"keywordMapping" + i + "_" + project.getId() + ".png");
		System.out.println("Generate WordCloud " + project.getName() + "Done");
	}

	public void generateSentimentChart(String id, Date start, Date end, Integer offset, Integer limit, int i) throws IOException {
		Project project = reportService.getById(id);
		List<SentimentPeakCounter> positiveList = analyticRepository.getSentimentPositiveEachDay(project, start, end);
		List<SentimentPeakCounter> negativeList = analyticRepository.getSentimentNegativeEachDay(project, start, end);
		List<SentimentPeakCounter> neutralList = analyticRepository.getSentimentNeutralEachDay(project, start, end);
		
		TimeSeries positive = new TimeSeries("Positive");
		TimeSeries neutral = new TimeSeries("Neutral");
		TimeSeries negative = new TimeSeries("Negative");

		for(SentimentPeakCounter counter : positiveList){
			SimpleDateFormat day = new SimpleDateFormat("dd");
			SimpleDateFormat month = new SimpleDateFormat("MM");
			SimpleDateFormat year = new SimpleDateFormat("yyy");
			
			positive.add(
					new Day(Integer.parseInt(day.format(counter.getPeakDay())),
							Integer.parseInt(month.format(counter.getPeakDay())),
							Integer.parseInt(year.format(counter.getPeakDay()))),
					counter.getCount());
		}
		
		for(SentimentPeakCounter counter : neutralList){
			SimpleDateFormat day = new SimpleDateFormat("dd");
			SimpleDateFormat month = new SimpleDateFormat("MM");
			SimpleDateFormat year = new SimpleDateFormat("yyy");
			
			neutral.add(
					new Day(Integer.parseInt(day.format(counter.getPeakDay())),
							Integer.parseInt(month.format(counter.getPeakDay())),
							Integer.parseInt(year.format(counter.getPeakDay()))),
					counter.getCount());
		}
		
		for(SentimentPeakCounter counter : negativeList){
			SimpleDateFormat day = new SimpleDateFormat("dd");
			SimpleDateFormat month = new SimpleDateFormat("MM");
			SimpleDateFormat year = new SimpleDateFormat("yyy");
			
			negative.add(
					new Day(Integer.parseInt(day.format(counter.getPeakDay())),
							Integer.parseInt(month.format(counter.getPeakDay())),
							Integer.parseInt(year.format(counter.getPeakDay()))),
					counter.getCount());
		}
		final TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(positive);
		dataset.addSeries(neutral);
		dataset.addSeries(negative);

		final JFreeChart chart = ChartFactory.createTimeSeriesChart("", "Date", "Total News", dataset, true, true,
				false);
		// final XYPlot plot = chart.getXYPlot();
		final XYPlot plot = (XYPlot) chart.getPlot();
		XYLineAndShapeRenderer r = (XYLineAndShapeRenderer) plot.getRenderer();
		r.setSeriesShape(0, ShapeUtilities.createDiamond(5));
		r.setSeriesShape(1, ShapeUtilities.createDiamond(5));
		r.setSeriesShape(2, ShapeUtilities.createDiamond(5));

		r.setSeriesShapesVisible(0, true);
		r.setSeriesShapesVisible(1, true);
		r.setSeriesShapesVisible(2, true);

		plot.setBackgroundPaint(Color.white);
		plot.setDomainGridlinePaint(Color.black);
		plot.setRangeGridlinePaint(Color.white);
		plot.getRenderer().setSeriesStroke(0, new BasicStroke(4));
		plot.getRenderer().setSeriesStroke(1, new BasicStroke(4));
		plot.getRenderer().setSeriesStroke(2, new BasicStroke(4));
		plot.getRenderer().setSeriesPaint(0, new Color(1686005));
		plot.getRenderer().setSeriesPaint(1, new Color(15395562));
		plot.getRenderer().setSeriesPaint(2, new Color(16748174));
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);
		plot.setOutlineVisible(false);
	    chart.getLegend().setFrame(BlockBorder.NONE);
	    
		final XYItemRenderer renderer = plot.getRenderer();
		if (renderer instanceof StandardXYItemRenderer) {
			final StandardXYItemRenderer rr = (StandardXYItemRenderer) renderer;
			rr.setPlotLines(true);
			rr.setBaseShapesFilled(true);
			rr.setBaseItemLabelsVisible(true);
		}

		final DateAxis axis = (DateAxis) plot.getDomainAxis();
		final TickUnits standardUnits = new TickUnits();
		standardUnits.add(new DateTickUnit(DateTickUnitType.DAY, 1, new SimpleDateFormat("MMM dd")));
		standardUnits.add(new DateTickUnit(DateTickUnitType.DAY, 7, new SimpleDateFormat("MMM dd")));
		standardUnits.add(new DateTickUnit(DateTickUnitType.MONTH, 1, new SimpleDateFormat("MMM ")));
		axis.setStandardTickUnits(standardUnits);
		int width = 710;
		int height = 430;
		File lineChart = new File(monthlyDir+"sentiment" + i + "_" + project.getId() + ".png");
		ChartUtilities.saveChartAsPNG(lineChart, chart, width, height);
		System.out.println("Generate sentimentChart " + project.getName() + "Done");
	}

	public void generateMediaReputation(String id, Date start, Date end, String accessToken, Integer offset, Integer limit, int i) throws IOException {
		String filePath = monthlyDir;
		String url = new StringBuilder().append(urlGnews).append(id)
				.append("/media-reputation?start=").append(dateFormat.format(start)).append("&end=")
				.append(dateFormat.format(end)).append("&accessToken=").append(accessToken).append("&limit=8")
				.append("&offset=0").toString();
		File dir = new File(filePath);
		if (!dir.exists()) {
			dir.mkdirs();
			log.debug("creating pdf upload directory");
		}
		String command = "wkhtmltoimage --quality 100 --javascript-delay 800 \"" + url + "\" " + filePath
				+ "mediareputationmonth" + i + "_" + id + ".jpg";
		log.info(executeCommand(command));
		log.info("finish");
	}

	private String executeCommand(String command) {
		StringBuffer output = new StringBuffer();
		Process p;
		try {
			p = Runtime.getRuntime().exec(new String[] { "bash", "-c", command });
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = "";
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}
			output.append("\n file created");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output.toString();
	}

}
