package io.gnews.pro.report.service;

import java.util.List;

import io.gnews.pro.core.model.mongodb.AutomatedReport;
import io.gnews.pro.core.model.mongodb.Project;


public interface ReportService {
	
	/**
	 * @param id
	 * @return
	 */
	public Project getById(String id);
	
	public List<Project> projectExpiredChecker();
	
	public List<AutomatedReport> getAllActiveProject();
	
	public List<AutomatedReport> getAllActiveMonthProject();
	
	

}
