package io.gnews.pro.report.dto;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;

public class DataMarksBeanList {
	@Autowired
	ResourceLoader resourceLoader;
		public ArrayList<DataMarksBean> getDataBeanList() throws IOException {
			
			ArrayList<DataMarksBean> dataBeanList = new ArrayList<DataMarksBean>();
			dataBeanList.add(produce("Ada Kodok"));
			return dataBeanList;
		}
	 
		private DataMarksBean produce(String data) {
			DataMarksBean dataBean = new DataMarksBean();
	 		dataBean.setTitle(data);
			
			return dataBean;
		}
	}
