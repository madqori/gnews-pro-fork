package io.gnews.pro.report.service.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.config.RabbitMQConfig;
import io.gnews.pro.core.model.dto.AddARMailSenderForm;
import io.gnews.pro.core.model.mongodb.AutomatedReport;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.repository.mongodb.AutomatedReportRepository;
import io.gnews.pro.core.util.DateUtils;
import io.gnews.pro.report.dto.DataMarksBean;
import io.gnews.pro.report.dto.DataMarksBeanList;
import io.gnews.pro.report.repository.WebDataReportRepository;
import io.gnews.pro.report.service.AutomatedReportService;
import io.gnews.pro.report.service.ReportService;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.ExporterInputItem;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleExporterInputItem;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

@Service
public class AutomatedReportServiceImpl implements AutomatedReportService {

	@Value("${report.data.weekly}")
	private String weeklyDir;
	@Value("${report.data.monthly}")
	private String monthlyDir;
	@Value("${report.data.doc}")
	private String docDir;
	
	@Value("${report.data.companylogo}")
	private String companyLogoDir;


	public static final String WEEKLY_NAME_PREFIX = "weekly_report";
	public static final String MONTHLY_NAME_PREFIX = "monthly_report";
	public static final String[] Subject = {"Weekly Report","Monthly Report"};
	public static final int WEEK = 4;
	public static final int IMAGE_SCALE_WIDTH = 280;
	public static final int IMAGE_SCALE_HEIGHT = 150;
	public static final String PPTX_TYPE = ".pptx";
	public static final String PDF_TYPE = ".pdf";

	@Autowired
	private ResourceLoader resourceLoader;

	@Autowired
	private ReportService reportService;

	@Autowired
	private WebDataReportRepository analyticRepository;

	@Autowired
	private ChartGenerator chartGenerator;

	@Autowired
	private ChartGeneratorMonthly chartGeneratorMonthly;

	@Autowired
	private AutomatedReportRepository automatedReportRepository;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	public void generatePDFJasperChart(String id, Date start, Date end, String accessToken, Integer offset, Integer limit)
			throws IOException {
		/* put some commend here to generate chart */
		chartGenerator.generateWordCloud(id, start, end);
		chartGenerator.generateTopMedia(id, start, end, offset, limit);
		chartGenerator.generateSentimentChart(id, start, end, offset, limit);
		chartGenerator.generatePeakDay(id, start, end, offset, limit);
		chartGenerator.generateMediaReputation(id, start, end, accessToken, offset, limit);

		SimpleDateFormat dateFormat = new SimpleDateFormat("E, MMM dd, yyyy");
		String startD = dateFormat.format(start);
		String endD = dateFormat.format(end);
		Project projects = reportService.getById(id);
		AutomatedReport automatedReport = automatedReportRepository.findByProject(projects);
		Long mediaCount = analyticRepository.getMediaCount(projects, start, end);
		Long newsCount = analyticRepository.getArticleCount(projects, start, end);

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("projectName", automatedReport.getProject().getName());
		parameters.put("keyword", automatedReport.getProject().getKeyword());
		parameters.put("period", new StringBuilder().append(startD).append(" - ").append(endD));
		parameters.put("countMedia", mediaCount);
		parameters.put("countNews", newsCount);
		List<ExporterInputItem> jpList = new ArrayList<>();
		try {
			Resource coverTemp = resourceLoader.getResource("classpath:reports/weekly-gnews-pro-cover.jasper");
			Resource executiveSum = resourceLoader.getResource("classpath:reports/weekly-executive-summary.jasper");
			Resource keywordTemp = resourceLoader.getResource("classpath:reports/weekly-keyword-mapping.jasper");
			Resource topMediaTemp = resourceLoader.getResource("classpath:reports/weekly-top-media.jasper");
			Resource sentimentTemp = resourceLoader.getResource("classpath:reports/weekly-sentiment.jasper");
			Resource mediaMappingTemp = resourceLoader.getResource("classpath:reports/weekly-media-reputation.jasper");
			Resource peakDayTemp = resourceLoader.getResource("classpath:reports/weekly-periodic-time.jasper");
			Resource endTemp = resourceLoader.getResource("classpath:reports/weekly-end-cover.jasper");

			if (automatedReport.getCompanyLogo() != null) {
				BufferedImage logo = ImageIO.read(new File(companyLogoDir + automatedReport.getCompanyLogo()));
				parameters.put("logo", logo);
			} else {
				Resource logoGnews = resourceLoader.getResource("classpath:reports/logo_gnewspro_big.png");
				BufferedImage logo = ImageIO.read(logoGnews.getInputStream());
				parameters.put("logo", logo);
			}
			File kmSourceImg = new File(
					weeklyDir + "keywordMapping" + automatedReport.getProject().getId() + ".png");
			BufferedImage keywordMapping = ImageIO.read(kmSourceImg);
			parameters.put("keywordMapping", keywordMapping);

			File topMediaImg = new File(weeklyDir + "topMedia" + automatedReport.getProject().getId() + ".png");
			BufferedImage topMediaBuf = ImageIO.read(topMediaImg);
			parameters.put("topMedia", topMediaBuf);

			File sentimentImg = new File(weeklyDir + "sentiment" + automatedReport.getProject().getId() + ".png");
			BufferedImage sentimentBuf = ImageIO.read(sentimentImg);
			parameters.put("sentiment", sentimentBuf);

			File mediaMappingImg = new File(
					weeklyDir + "mediareputation" + automatedReport.getProject().getId() + ".jpg");
			BufferedImage mediaMappingBuf = ImageIO.read(mediaMappingImg);
			parameters.put("mediaReputation", mediaMappingBuf);

			File peakTimeImg = new File(weeklyDir + "peakDay" + automatedReport.getProject().getId() + ".png");
			BufferedImage peakTimeBuf = ImageIO.read(peakTimeImg);
			parameters.put("peakDay", peakTimeBuf);

			DataMarksBeanList DataBeanList = new DataMarksBeanList();
			ArrayList<DataMarksBean> dataList = DataBeanList.getDataBeanList();
			JRBeanCollectionDataSource one = new JRBeanCollectionDataSource(dataList);
			JRBeanCollectionDataSource two = new JRBeanCollectionDataSource(dataList);
			JRBeanCollectionDataSource three = new JRBeanCollectionDataSource(dataList);
			JRBeanCollectionDataSource four = new JRBeanCollectionDataSource(dataList);
			JRBeanCollectionDataSource five = new JRBeanCollectionDataSource(dataList);
			JRBeanCollectionDataSource six = new JRBeanCollectionDataSource(dataList);
			JRBeanCollectionDataSource seven = new JRBeanCollectionDataSource(dataList);
			JRBeanCollectionDataSource eight = new JRBeanCollectionDataSource(dataList);

			// cover
			JasperReport cover = (JasperReport) JRLoader.loadObject(coverTemp.getInputStream());
			JasperPrint coverPrint = JasperFillManager.fillReport(cover, parameters, one);

			JasperReport exSummary = (JasperReport) JRLoader.loadObject(executiveSum.getInputStream());
			JasperPrint exSummaryPrint = JasperFillManager.fillReport(exSummary, parameters, two);

			JasperReport keyword = (JasperReport) JRLoader.loadObject(keywordTemp.getInputStream());
			JasperPrint keywordPrint = JasperFillManager.fillReport(keyword, parameters, three);

			JasperReport topMedia = (JasperReport) JRLoader.loadObject(topMediaTemp.getInputStream());
			JasperPrint topMediaPrint = JasperFillManager.fillReport(topMedia, parameters, four);

			JasperReport sentiment = (JasperReport) JRLoader.loadObject(sentimentTemp.getInputStream());
			JasperPrint sentimentPrint = JasperFillManager.fillReport(sentiment, parameters, five);

			JasperReport mediaMapping = (JasperReport) JRLoader.loadObject(mediaMappingTemp.getInputStream());
			JasperPrint mediaMappingPrint = JasperFillManager.fillReport(mediaMapping, parameters, six);

			JasperReport peakDay = (JasperReport) JRLoader.loadObject(peakDayTemp.getInputStream());
			JasperPrint peakDayPrint = JasperFillManager.fillReport(peakDay, parameters, seven);

			JasperReport endCover = (JasperReport) JRLoader.loadObject(endTemp.getInputStream());
			JasperPrint endCoverPrint = JasperFillManager.fillReport(endCover, parameters, eight);

			jpList.add(new SimpleExporterInputItem(coverPrint));
			jpList.add(new SimpleExporterInputItem(exSummaryPrint));
			jpList.add(new SimpleExporterInputItem(keywordPrint));
			jpList.add(new SimpleExporterInputItem(topMediaPrint));
			jpList.add(new SimpleExporterInputItem(sentimentPrint));
			jpList.add(new SimpleExporterInputItem(mediaMappingPrint));
			jpList.add(new SimpleExporterInputItem(peakDayPrint));
			jpList.add(new SimpleExporterInputItem(endCoverPrint));

			// Add count 1 after accessed weekly report
			AutomatedReport automReportMont = automatedReportRepository.findByProject(projects);
			automReportMont.setMonthlyCount(automReportMont.getMonthlyCount() + 1);
			automatedReportRepository.save(automReportMont);
			if (automatedReport.getReportType().equals(PPTX_TYPE) | automatedReport.getReportType() == null) {
				JRPptxExporter exporterPPTMonth = new JRPptxExporter();
				exporterPPTMonth.setExporterInput(new SimpleExporterInput(jpList));
				OutputStream output = new FileOutputStream(
						new File(docDir + WEEKLY_NAME_PREFIX + automatedReport.getProject().getId() + PPTX_TYPE));
				exporterPPTMonth.setExporterOutput(new SimpleOutputStreamExporterOutput(output));
				exporterPPTMonth.exportReport();
				// Email Sender
				if (automatedReport.getEmail() == null) {
					AddARMailSenderForm mailForm = new AddARMailSenderForm();
					mailForm.setId(projects.getId());
					mailForm.setTo(automatedReport.getProject().getUser().getEmail());
					mailForm.setTypeDoc(PPTX_TYPE);
					mailForm.setSubject(Subject[0]);
					mailForm.setDocName("Weekly Report " + automatedReport.getProject().getId() + PPTX_TYPE);
					mailForm.setAttachment(docDir+WEEKLY_NAME_PREFIX + automatedReport.getProject().getId() + PPTX_TYPE);
					
					rabbitTemplate.convertAndSend(RabbitMQConfig.SEND_MAIL_AR, mailForm);
				} else {
					AddARMailSenderForm mailForm = new AddARMailSenderForm();
					mailForm.setId(projects.getId());
					mailForm.setTo(automatedReport.getEmail());
					mailForm.setTypeDoc(PPTX_TYPE);
					mailForm.setSubject(Subject[0]);
					mailForm.setDocName("Weekly Report " + automatedReport.getProject().getId() + PPTX_TYPE);
					mailForm.setAttachment(docDir+WEEKLY_NAME_PREFIX + automatedReport.getProject().getId() + PPTX_TYPE);
					
					rabbitTemplate.convertAndSend(RabbitMQConfig.SEND_MAIL_AR, mailForm);
				}
			} else if (automatedReport.getReportType().equals(PDF_TYPE)) {
				JRPdfExporter exporterPDFMonth = new JRPdfExporter();
				exporterPDFMonth.setExporterInput(new SimpleExporterInput(jpList));
				OutputStream output = new FileOutputStream(
						new File(docDir + WEEKLY_NAME_PREFIX + automatedReport.getProject().getId() + PDF_TYPE));
				exporterPDFMonth.setExporterOutput(new SimpleOutputStreamExporterOutput(output));
				exporterPDFMonth.exportReport();

				if (automatedReport.getEmail() == null) {
					AddARMailSenderForm mailForm = new AddARMailSenderForm();
					mailForm.setId(projects.getId());
					mailForm.setTo(automatedReport.getProject().getUser().getEmail());
					mailForm.setTypeDoc(PDF_TYPE);
					mailForm.setSubject(Subject[0]);
					mailForm.setDocName("Weekly Report " + automatedReport.getProject().getId() + PDF_TYPE);
					mailForm.setAttachment(docDir+WEEKLY_NAME_PREFIX + automatedReport.getProject().getId() + PDF_TYPE);
					
					rabbitTemplate.convertAndSend(RabbitMQConfig.SEND_MAIL_AR, mailForm);
				} else {
					AddARMailSenderForm mailForm = new AddARMailSenderForm();
					mailForm.setId(projects.getId());
					mailForm.setTo(automatedReport.getEmail());
					mailForm.setTypeDoc(PDF_TYPE);
					mailForm.setSubject(Subject[0]);
					mailForm.setDocName("Weekly Report " + automatedReport.getProject().getId() + PDF_TYPE);
					mailForm.setAttachment(docDir+WEEKLY_NAME_PREFIX+ automatedReport.getProject().getId() + PDF_TYPE);
					
					rabbitTemplate.convertAndSend(RabbitMQConfig.SEND_MAIL_AR, mailForm);
				}
			}
		} catch (JRException e) {
			e.printStackTrace();
		}
	}

	// generator Automated Report
	public void generatePDFJasperChartMonthly(String id, Date end, String accessToken, Integer offset, Integer limit)
			throws IOException {

		/* put some commend here to generate chart */
		Project projects = reportService.getById(id);
		AutomatedReport automatedReport = automatedReportRepository.findByProject(projects);
		Date newEnd = end;
		Date newStart = DateUtils.before(newEnd, 7);
		Date startPeriod = DateUtils.before(end, 28);
		SimpleDateFormat dateFormat = new SimpleDateFormat("E, MMM dd, yyyy");
		List<StringBuilder> period = new ArrayList<StringBuilder>();
		for (int i = 1; i <= WEEK; i++) {
			period.add(new StringBuilder().append(dateFormat.format(newStart)).append(" - ")
					.append(dateFormat.format(newEnd)));
			chartGeneratorMonthly.generateWordCloud(id, newStart, newEnd, i);
			chartGeneratorMonthly.generateTopMedia(id, newStart, newEnd, offset, limit, i);
			chartGeneratorMonthly.generateSentimentChart(id, newStart, newEnd, offset, limit, i);
			chartGeneratorMonthly.generatePeakDay(id, newStart, newEnd, offset, limit, i);
			chartGeneratorMonthly.generateMediaReputation(id, newStart, newEnd, accessToken, offset, limit, i);
			newEnd = DateUtils.before(newEnd, 7);
			newStart = DateUtils.before(newEnd, 7);
		}

		// Count Total Media and Total News
		Long mediaCount = analyticRepository.getMediaMonthlyCount(projects, startPeriod, end);
		Long newsCount = analyticRepository.getArticleMonthlyCount(projects, startPeriod, end);

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("projectName", automatedReport.getProject().getName());
		parameters.put("keyword", automatedReport.getProject().getKeyword());
		parameters.put("period", new StringBuilder().append(dateFormat.format(startPeriod)).append(" - ")
				.append(dateFormat.format(end)));

		parameters.put("countMedia", mediaCount);
		parameters.put("countNews", newsCount);

		// set Parameter for each period
		parameters.put("period_week_1", period.get(3));
		parameters.put("period_week_2", period.get(2));
		parameters.put("period_week_3", period.get(1));
		parameters.put("period_week_4", period.get(0));

		List<ExporterInputItem> jpListMonth = new ArrayList<>();
		try {
			Resource coverMonthTemp = resourceLoader.getResource("classpath:reports/monthly-gnews-pro-cover.jasper");
			Resource executiveMonthSum = resourceLoader
					.getResource("classpath:reports/monthly-executive-summary.jasper");
			Resource keywordMonthTemp = resourceLoader.getResource("classpath:reports/monthly-keyword-mapping.jasper");
			Resource topMediaMonthTemp = resourceLoader.getResource("classpath:reports/monthly-top-media.jasper");
			Resource sentimentMonthTemp = resourceLoader.getResource("classpath:reports/monthly-sentiment.jasper");
			Resource mediaMappingMonthTemp = resourceLoader
					.getResource("classpath:reports/monthly-media-reputation.jasper");
			Resource peakDayMonthTemp = resourceLoader.getResource("classpath:reports/monthly-periodic-time.jasper");
			Resource endMonthTemp = resourceLoader.getResource("classpath:reports/monthly-end-cover.jasper");

			if (automatedReport.getCompanyLogo() != null) {
				BufferedImage logo = ImageIO.read(new File(companyLogoDir + automatedReport.getCompanyLogo()));
				parameters.put("logoMonth", logo);
			} else {
				Resource logoGnews = resourceLoader.getResource("classpath:reports/logo_gnewspro_big.png");
				BufferedImage logo = ImageIO.read(logoGnews.getInputStream());
				parameters.put("logoMonth", logo);
			}
			File kmSourceImgA = new File(
					monthlyDir + "keywordMapping1_" + automatedReport.getProject().getId() + ".png");
			BufferedImage keywordMappingA = ImageIO.read(kmSourceImgA);
			parameters.put("keywordMappingMonthA", keywordMappingA);

			File kmSourceImgB = new File(
					monthlyDir + "keywordMapping2_" + automatedReport.getProject().getId() + ".png");
			BufferedImage keywordMappingB = ImageIO.read(kmSourceImgB);
			parameters.put("keywordMappingMonthB", keywordMappingB);

			File kmSourceImgC = new File(
					monthlyDir + "keywordMapping3_" + automatedReport.getProject().getId() + ".png");
			BufferedImage keywordMappingC = ImageIO.read(kmSourceImgC);
			parameters.put("keywordMappingMonthC", keywordMappingC);

			File kmSourceImgD = new File(
					monthlyDir + "keywordMapping4_" + automatedReport.getProject().getId() + ".png");
			BufferedImage keywordMappingD = ImageIO.read(kmSourceImgD);
			parameters.put("keywordMappingMonthD", keywordMappingD);

			File topMediaImgA = new File(
					monthlyDir + "topMedia1_" + automatedReport.getProject().getId() + ".png");
			BufferedImage topMediaBufA = ImageIO.read(topMediaImgA);
			parameters.put("topMediaMonthA", topMediaBufA);

			File topMediaImgB = new File(
					monthlyDir + "topMedia2_" + automatedReport.getProject().getId() + ".png");
			BufferedImage topMediaBufB = ImageIO.read(topMediaImgB);
			parameters.put("topMediaMonthB", topMediaBufB);

			File topMediaImgC = new File(
					monthlyDir + "topMedia3_" + automatedReport.getProject().getId() + ".png");
			BufferedImage topMediaBufC = ImageIO.read(topMediaImgC);
			parameters.put("topMediaMonthC", topMediaBufC);

			File topMediaImgD = new File(
					monthlyDir + "topMedia4_" + automatedReport.getProject().getId() + ".png");
			BufferedImage topMediaBufD = ImageIO.read(topMediaImgD);
			parameters.put("topMediaMonthD", topMediaBufD);

			File sentimentImgA = new File(
					monthlyDir + "sentiment1_" + automatedReport.getProject().getId() + ".png");
			BufferedImage sentimentBufA = ImageIO.read(sentimentImgA);
			parameters.put("sentimentMonthA", sentimentBufA);

			File sentimentImgB = new File(
					monthlyDir + "sentiment2_" + automatedReport.getProject().getId() + ".png");
			BufferedImage sentimentBufB = ImageIO.read(sentimentImgB);
			parameters.put("sentimentMonthB", sentimentBufB);

			File sentimentImgC = new File(
					monthlyDir + "sentiment3_" + automatedReport.getProject().getId() + ".png");
			BufferedImage sentimentBufC = ImageIO.read(sentimentImgC);
			parameters.put("sentimentMonthC", sentimentBufC);

			File sentimentImgD = new File(
					monthlyDir + "sentiment4_" + automatedReport.getProject().getId() + ".png");
			BufferedImage sentimentBufD = ImageIO.read(sentimentImgD);
			parameters.put("sentimentMonthD", sentimentBufD);

			File mediaMappingImgA = new File(
					monthlyDir + "mediareputationmonth1_" + automatedReport.getProject().getId() + ".jpg");
			BufferedImage mediaMappingBufA = ImageIO.read(mediaMappingImgA);
			parameters.put("mediaReputationMonthA", mediaMappingBufA);

			File mediaMappingImgB = new File(
					monthlyDir + "mediareputationmonth2_" + automatedReport.getProject().getId() + ".jpg");
			BufferedImage mediaMappingBufB = ImageIO.read(mediaMappingImgB);
			parameters.put("mediaReputationMonthB", mediaMappingBufB);

			File mediaMappingImgC = new File(
					monthlyDir + "mediareputationmonth3_" + automatedReport.getProject().getId() + ".jpg");
			BufferedImage mediaMappingBufC = ImageIO.read(mediaMappingImgC);
			parameters.put("mediaReputationMonthC", mediaMappingBufC);

			File mediaMappingImgD = new File(
					monthlyDir + "mediareputationmonth4_" + automatedReport.getProject().getId() + ".jpg");
			BufferedImage mediaMappingBufD = ImageIO.read(mediaMappingImgD);
			parameters.put("mediaReputationMonthD", mediaMappingBufD);

			File peakTimeImgA = new File(monthlyDir + "peakDay1_" + automatedReport.getProject().getId() + ".png");
			BufferedImage peakTimeBufA = ImageIO.read(peakTimeImgA);
			parameters.put("peakDayMonthA", peakTimeBufA);

			File peakTimeImgB = new File(monthlyDir + "peakDay2_" + automatedReport.getProject().getId() + ".png");
			BufferedImage peakTimeBufB = ImageIO.read(peakTimeImgB);
			parameters.put("peakDayMonthB", peakTimeBufB);

			File peakTimeImgC = new File(monthlyDir + "peakDay3_" + automatedReport.getProject().getId() + ".png");
			BufferedImage peakTimeBufC = ImageIO.read(peakTimeImgC);
			parameters.put("peakDayMonthC", peakTimeBufC);

			File peakTimeImgD = new File(monthlyDir + "peakDay4_" + automatedReport.getProject().getId() + ".png");
			BufferedImage peakTimeBufD = ImageIO.read(peakTimeImgD);
			parameters.put("peakDayMonthD", peakTimeBufD);

			DataMarksBeanList DataBeanListMonth = new DataMarksBeanList();
			ArrayList<DataMarksBean> dataListMonth = DataBeanListMonth.getDataBeanList();
			JRBeanCollectionDataSource oneMonth = new JRBeanCollectionDataSource(dataListMonth);
			JRBeanCollectionDataSource twoMonth = new JRBeanCollectionDataSource(dataListMonth);
			JRBeanCollectionDataSource threeMonth = new JRBeanCollectionDataSource(dataListMonth);
			JRBeanCollectionDataSource fourMonth = new JRBeanCollectionDataSource(dataListMonth);
			JRBeanCollectionDataSource fiveMonth = new JRBeanCollectionDataSource(dataListMonth);
			JRBeanCollectionDataSource sixMonth = new JRBeanCollectionDataSource(dataListMonth);
			JRBeanCollectionDataSource sevenMonth = new JRBeanCollectionDataSource(dataListMonth);
			JRBeanCollectionDataSource eightMonth = new JRBeanCollectionDataSource(dataListMonth);

			// cover
			JasperReport coverMonth = (JasperReport) JRLoader.loadObject(coverMonthTemp.getInputStream());
			JasperPrint coverMonthPrint = JasperFillManager.fillReport(coverMonth, parameters, oneMonth);

			JasperReport exSummaryMonth = (JasperReport) JRLoader.loadObject(executiveMonthSum.getInputStream());
			JasperPrint exSummaryMonthPrint = JasperFillManager.fillReport(exSummaryMonth, parameters, twoMonth);

			JasperReport keywordMonth = (JasperReport) JRLoader.loadObject(keywordMonthTemp.getInputStream());
			JasperPrint keywordMonthPrint = JasperFillManager.fillReport(keywordMonth, parameters, threeMonth);

			JasperReport topMediaMonth = (JasperReport) JRLoader.loadObject(topMediaMonthTemp.getInputStream());
			JasperPrint topMediaMonthPrint = JasperFillManager.fillReport(topMediaMonth, parameters, fourMonth);

			JasperReport sentimentMonth = (JasperReport) JRLoader.loadObject(sentimentMonthTemp.getInputStream());
			JasperPrint sentimentMonthPrint = JasperFillManager.fillReport(sentimentMonth, parameters, fiveMonth);

			JasperReport mediaMonthMapping = (JasperReport) JRLoader.loadObject(mediaMappingMonthTemp.getInputStream());
			JasperPrint mediaMappingMonthPrint = JasperFillManager.fillReport(mediaMonthMapping, parameters, sixMonth);

			JasperReport peakMonthDay = (JasperReport) JRLoader.loadObject(peakDayMonthTemp.getInputStream());
			JasperPrint peakDayMonthPrint = JasperFillManager.fillReport(peakMonthDay, parameters, sevenMonth);

			JasperReport endMonthCover = (JasperReport) JRLoader.loadObject(endMonthTemp.getInputStream());
			JasperPrint endCoverMonthPrint = JasperFillManager.fillReport(endMonthCover, parameters, eightMonth);

			jpListMonth.add(new SimpleExporterInputItem(coverMonthPrint));
			jpListMonth.add(new SimpleExporterInputItem(exSummaryMonthPrint));
			jpListMonth.add(new SimpleExporterInputItem(keywordMonthPrint));
			jpListMonth.add(new SimpleExporterInputItem(topMediaMonthPrint));
			jpListMonth.add(new SimpleExporterInputItem(sentimentMonthPrint));
			jpListMonth.add(new SimpleExporterInputItem(mediaMappingMonthPrint));
			jpListMonth.add(new SimpleExporterInputItem(peakDayMonthPrint));
			jpListMonth.add(new SimpleExporterInputItem(endCoverMonthPrint));

			// reset Monthly Count to zero
			automatedReport.setMonthlyCount(0);
			automatedReportRepository.save(automatedReport);

			if (automatedReport.getReportType().equals(PPTX_TYPE) | automatedReport.getReportType() == null) {
				JRPptxExporter exporterPPTMonth = new JRPptxExporter();
				exporterPPTMonth.setExporterInput(new SimpleExporterInput(jpListMonth));
				OutputStream output = new FileOutputStream(
						new File(docDir + MONTHLY_NAME_PREFIX + automatedReport.getProject().getId() + PPTX_TYPE));
				exporterPPTMonth.setExporterOutput(new SimpleOutputStreamExporterOutput(output));
				exporterPPTMonth.exportReport();
				// Email Sender
				if (automatedReport.getEmail() == null) {
					AddARMailSenderForm mailForm = new AddARMailSenderForm();
					mailForm.setId(projects.getId());
					mailForm.setTo(automatedReport.getProject().getUser().getEmail());
					mailForm.setTypeDoc(PPTX_TYPE);
					mailForm.setSubject(Subject[1]);
					mailForm.setDocName("Monthly Report " + automatedReport.getProject().getId() + PPTX_TYPE);
					mailForm.setAttachment(docDir+MONTHLY_NAME_PREFIX + automatedReport.getProject().getId() + PPTX_TYPE);
					
					rabbitTemplate.convertAndSend(RabbitMQConfig.SEND_MAIL_AR, mailForm);
				} else {
					AddARMailSenderForm mailForm = new AddARMailSenderForm();
					mailForm.setId(projects.getId());
					mailForm.setTo(automatedReport.getEmail());
					mailForm.setTypeDoc(PPTX_TYPE);
					mailForm.setSubject(Subject[1]);
					mailForm.setDocName("Monthly Report " + automatedReport.getProject().getId() + PPTX_TYPE);
					mailForm.setAttachment(docDir+MONTHLY_NAME_PREFIX + automatedReport.getProject().getId() + PPTX_TYPE);
					
					rabbitTemplate.convertAndSend(RabbitMQConfig.SEND_MAIL_AR, mailForm);
				}
			} else if (automatedReport.getReportType().equals(PDF_TYPE)) {
				JRPdfExporter exporterPDFMonth = new JRPdfExporter();
				exporterPDFMonth.setExporterInput(new SimpleExporterInput(jpListMonth));
				OutputStream output = new FileOutputStream(
						new File(docDir + MONTHLY_NAME_PREFIX + automatedReport.getProject().getId() + PDF_TYPE));
				exporterPDFMonth.setExporterOutput(new SimpleOutputStreamExporterOutput(output));
				exporterPDFMonth.exportReport();

				if (automatedReport.getEmail() == null) {
					AddARMailSenderForm mailForm = new AddARMailSenderForm();
					mailForm.setId(projects.getId());
					mailForm.setTo(automatedReport.getProject().getUser().getEmail());
					mailForm.setTypeDoc(PDF_TYPE);
					mailForm.setSubject(Subject[1]);
					mailForm.setDocName("Monthly Report " + automatedReport.getProject().getId() + PDF_TYPE);
					mailForm.setAttachment(docDir+MONTHLY_NAME_PREFIX + automatedReport.getProject().getId() + PDF_TYPE);
					
					rabbitTemplate.convertAndSend(RabbitMQConfig.SEND_MAIL_AR, mailForm);
				} else {
					AddARMailSenderForm mailForm = new AddARMailSenderForm();
					mailForm.setId(projects.getId());
					mailForm.setTo(automatedReport.getEmail());
					mailForm.setTypeDoc(PDF_TYPE);
					mailForm.setSubject(Subject[1]);
					mailForm.setDocName("Monthly Report " + automatedReport.getProject().getId() + PDF_TYPE);
					mailForm.setAttachment(docDir+MONTHLY_NAME_PREFIX+ automatedReport.getProject().getId() + PDF_TYPE);
					
					rabbitTemplate.convertAndSend(RabbitMQConfig.SEND_MAIL_AR, mailForm);
				}
			}
		} catch (JRException e) {
			e.printStackTrace();
		}
	}

}
