package io.gnews.pro.report.service;

import java.io.IOException;
import java.util.Date;

public interface AutomatedReportService {
	
	public void generatePDFJasperChart(
			String id,
			Date start,
    		Date end,
   		    String accessToken,
    		Integer offset,
   		    Integer limit
			) throws IOException;
	
	public void generatePDFJasperChartMonthly(
			String id,
			Date end,
   		    String accessToken,
    		Integer offset,
   		    Integer limit
			) throws IOException;
	
}
