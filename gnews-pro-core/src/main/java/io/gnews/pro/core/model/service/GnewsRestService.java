package io.gnews.pro.core.model.service;

import java.util.List;

import io.gnews.pro.core.model.mongodb.WebData;

public interface GnewsRestService {

	public boolean createAnalyticKeyword(String keyword);
	
	public String getKeywordImage(String keyword);

	public List<WebData> getData(
			String keyword, 
			String language,
			String filter,
			Integer limit, 
			Integer offset 
			);
	
}
