package io.gnews.pro.core.repository.mongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.EmailSender;

public interface EmailSenderRepository extends MongoRepository<EmailSender, String> {

	public EmailSender findByEmail(String email);

	public EmailSender findByProjectIdAndEmail(String projectId, String email);

	public List<EmailSender> findByProjectId(String projectId);

	public List<EmailSender> findByProjectIdAndValidTrue(String projectId);

	public EmailSender findByValidationToken(String token);

}
