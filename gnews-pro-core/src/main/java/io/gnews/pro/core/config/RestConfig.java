package io.gnews.pro.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author masasdani
 * Created Date Nov 8, 2015
 */
@Configuration
public class RestConfig {

	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}
	
}
