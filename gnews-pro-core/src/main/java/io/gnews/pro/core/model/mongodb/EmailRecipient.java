package io.gnews.pro.core.model.mongodb;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@SuppressWarnings("serial")
@Document(collection = "email_recipient")
public class EmailRecipient implements Serializable {

	@Id
	private String id;
	@Indexed(unique = true)
	private String email;
	private String name;
	private String firstName;
	private String lastName;
	private int bouncedCount = 0;
	
	public EmailRecipient() {
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getBouncedCount() {
		return bouncedCount;
	}

	public void setBouncedCount(int bouncedCount) {
		this.bouncedCount = bouncedCount;
	}
	
}
