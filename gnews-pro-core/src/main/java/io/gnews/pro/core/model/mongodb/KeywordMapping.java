package io.gnews.pro.core.model.mongodb;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import io.gnews.pro.core.model.dto.TermCounter;

@Document(collection = "keyword_mapping")
public class KeywordMapping {

	@Id
	private String id;
	private String projectId;
	private DateTime date;
	private List<TermCounter> termCounter;
	
	public KeywordMapping() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public DateTime getDate() {
		return date;
	}


	public void setDate(DateTime date) {
		this.date = date;
	}


	public List<TermCounter> getTermCounter() {
		return termCounter;
	}

	public void setTermCounter(List<TermCounter> termCounter) {
		this.termCounter = termCounter;
	}
	
}
