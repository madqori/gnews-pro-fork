package io.gnews.pro.core.repository.mongodb;


import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.model.mongodb.UserAccount;

/**
 * @author masasdani
 * Created Date Oct 27, 2015
 */
public interface UserAccountRepository extends MongoRepository<UserAccount, String> {

	/**
	 * @param username
	 * @return
	 */
	public UserAccount findByAccountEmailAndAccountActiveTrue(String username);

	/**
	 * @param userName
	 * @return
	 */
	public UserAccount findByAccountEmail(String userName);
	
	/**
	 * @param account
	 * @return
	 */
	public UserAccount findByAccount(Account account);
	
}
