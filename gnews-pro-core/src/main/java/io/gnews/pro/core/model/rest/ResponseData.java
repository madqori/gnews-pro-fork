package io.gnews.pro.core.model.rest;

/**
 * @author masasdani
 * Created Date Oct 27, 2015
 * @param <T>
 */
public class ResponseData<T> {

	private String status;
	private String message;
	private T data;

	public ResponseData() {

	}

	public ResponseData(T data) {
		this.data = data;
	}

	public ResponseData(String status, String message) {
		this.status = status;
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
