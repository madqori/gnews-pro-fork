package io.gnews.pro.core.model.mongodb;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import io.gnews.pro.core.config.DateDeserializer;
import io.gnews.pro.core.model.dto.ArticleData;

/**
 * @author masasdani
 * Created Date Nov 4, 2015
 */
@Document(collection = "web_data")
public class WebData {

	public static final int POSITIVE_SENTIMENT = 1;
	public static final int NEGATIVE_SENTIMENT = -1;
	public static final int NEUTRAL_SENTIMENT = 0;
		
	@Id
	private String url;
	@Indexed(unique = true)
	private String name;
	private String lang;
	private String host;
	private String domain;
	@TextIndexed
	private String title;
	private String image;
	private String favicon;
	@JsonDeserialize(using = DateDeserializer.class)
	@Indexed(background = true, direction = IndexDirection.DESCENDING)
	private Date date;
	private String snippet;
	private String content;
	@Indexed(background = true)
	private List<String> terms;
	@Indexed(background = true)
	private List<String> positives;
	@Indexed(background = true)
	private List<String> negatives;
	private int sentiment;
	private long peakDay;
	private long peakHour;
	
	public WebData() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getSnippet() {
		return snippet;
	}

	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}
	
	public List<String> getTerms() {
		return terms;
	}

	public void setTerms(List<String> terms) {
		this.terms = terms;
	}

	public int getSentiment() {
		return sentiment;
	}

	public void setSentiment(int sentiment) {
		this.sentiment = sentiment;
	}

	public long getPeakDay() {
		return peakDay;
	}

	public void setPeakDay(long peakDay) {
		this.peakDay = peakDay;
	}

	public long getPeakHour() {
		return peakHour;
	}

	public void setPeakHour(long peakHour) {
		this.peakHour = peakHour;
	}

	public List<String> getPositives() {
		return positives;
	}

	public void setPositives(List<String> positives) {
		this.positives = positives;
	}

	public List<String> getNegatives() {
		return negatives;
	}

	public void setNegatives(List<String> negatives) {
		this.negatives = negatives;
	}
	
	public String getFavicon() {
		return favicon;
	}

	public void setFavicon(String favicon) {
		this.favicon = favicon;
	}

	public ArticleData toArticle() {
		ArticleData article = new ArticleData();
		article.setId(name);
		article.setUrl(url);
		article.setDate(date.getTime() / 1000);
		article.setHost(host);
		article.setDomain(domain);
		article.setImage(image);
		article.setFavicon(favicon);
		article.setTitle(title);
		article.setContent(content);
		return article;
	}

	public ArticleData toArticleSimple() {
		ArticleData article = new ArticleData();
		article.setId(name);
		article.setUrl(url);
		article.setDate(date.getTime() / 1000);
		article.setHost(host);
		article.setDomain(domain);
		article.setImage(image);
		article.setFavicon(favicon);
		article.setTitle(title);
		article.setContent(snippet);
		return article;
	}

}
