package io.gnews.pro.core.model.dto;

import org.springframework.data.annotation.Id;

public class SentimentPeakCounter2{

	@Id
	private Long peakDay;
	SentimentCounter value;
	
	public SentimentPeakCounter2() {
	}

	public Long getPeakDay() {
		return peakDay;
	}

	public void setPeakDay(Long peakDay) {
		this.peakDay = peakDay;
	}

	public SentimentCounter getValue() {
		return value;
	}

	public void setValue(SentimentCounter value) {
		this.value = value;
	}
	
}
