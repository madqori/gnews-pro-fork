package io.gnews.pro.core.model.dto;

import org.springframework.data.annotation.Id;

/**
 * @author masasdani
 * Created Date Nov 3, 2015
 */
public class TermCounter2 implements Comparable<TermCounter2> {

	@Id
	private String term;
	private int count;
	
	public TermCounter2() {
	}

	public TermCounter2(String term, int count) {
		super();
		this.term = term;
		this.count = count;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int compareTo(TermCounter2 o) {
		return o.getCount() - this.count;
	}
	
}
