package io.gnews.pro.core.repository.mongodb;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.KeywordMapping;

public interface KeywordMappingRepository extends MongoRepository<KeywordMapping, String>{

}
