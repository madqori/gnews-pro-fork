package io.gnews.pro.core.model.mongodb;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@SuppressWarnings("serial")
@Document(collection = "email_message")
public class EmailMessage implements Serializable {

	@Id
	private String id;
	@Indexed(unique = true)
	private String messageId;
	@Indexed(unique = true)
	private String groupId;
	private String subject;
	@DBRef
	private EmailSender sender;
	@DBRef
	private EmailGroup emailGroup;
	private String content;
	private String htmlContent;
	private Date sendDate;
	private List<String> attachments;
	private String projectId;
	private int sentCount = 0;
	
	public EmailMessage() {
		if(sendDate == null) sendDate = new Date();
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public EmailSender getSender() {
		return sender;
	}

	public void setSender(EmailSender sender) {
		this.sender = sender;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	
	public List<String> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<String> attachments) {
		this.attachments = attachments;
	}
	
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public EmailGroup getEmailGroup() {
		return emailGroup;
	}

	public void setEmailGroup(EmailGroup emailGroup) {
		this.emailGroup = emailGroup;
	}

	public String getHtmlContent() {
		return htmlContent;
	}

	public void setHtmlContent(String htmlContent) {
		this.htmlContent = htmlContent;
	}

	public int getSentCount() {
		return sentCount;
	}

	public void setSentCount(int sentCount) {
		this.sentCount = sentCount;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

}
