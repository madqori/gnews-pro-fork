package io.gnews.pro.core.model.mongodb;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * class for storing user data in jpa
 * 
 * @author madqori Created Date Jan 26, 2016
 */
@SuppressWarnings("serial")
@Document(collection = "promo_code")
public class PromoCode implements Serializable {

	public static final String AGENT_USER = "AGENT";
	public static final String GDI_USER = "GDI";

	@Id
	private String id;
	@Indexed(unique = true)
	private String code;
	private Long price;
	private Date expiredAt;
	private Date createdAt;
	@DBRef
	private AgentAccount agent;
	private int period;
	private boolean expired = false;
	private boolean submited = false;

	public PromoCode() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Date getExpiredAt() {
		return expiredAt;
	}

	public void setExpiredAt(Date expiredAt) {
		this.expiredAt = expiredAt;
	}

	public AgentAccount getAgent() {
		return agent;
	}

	public void setAgent(AgentAccount agent) {
		this.agent = agent;
	}

	public boolean isSubmited() {
		return submited;
	}

	public void setSubmited(boolean submited) {
		this.submited = submited;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

}
