package io.gnews.pro.core.model.dto;

import org.joda.time.DateTime;

public class RankCounter {

	private String domain;
	private int globalRank;
	private DateTime date;
	private String countryName;
	private int countryRank;
	
	public RankCounter() {
	
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	
	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public int getGlobalRank() {
		return globalRank;
	}

	public void setGlobalRank(int globalRank) {
		this.globalRank = globalRank;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getCountryRank() {
		return countryRank;
	}

	public void setCountryRank(int countryRank) {
		this.countryRank = countryRank;
	}
	
}
