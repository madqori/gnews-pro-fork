package io.gnews.pro.core.util;

import java.util.Calendar;
import java.util.Date;

/**
 * @author masasdani
 * Created Date Oct 27, 2015
 */
public class DateUtils {

	/**
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date before(Date date, int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, -days);
		return calendar.getTime();
	}

	/**
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date after(Date date, int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, days);
		return calendar.getTime();
	}
	
	/**
	 * @param date
	 * @param month
	 * @return
	 */
	public static Date Monthafter(Date date, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, month);
		return calendar.getTime();
	}

	/**
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static int calculateNumberOfDaysBetween(Date startDate, Date endDate) {
		if (startDate.after(endDate)) {
			throw new IllegalArgumentException(
					"End date should be grater or equals to start date");
		}

		long startDateTime = startDate.getTime();
		long endDateTime = endDate.getTime();
		long milPerDay = 1000 * 60 * 60 * 24;

		int numOfDays = (int) ((endDateTime - startDateTime) / milPerDay); // calculate
																			// vacation
																			// duration
																			// in
																			// days

		return (numOfDays); // add one day to include start date in interval
	}

	/**
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static int calculateNumberOfDays(Date startDate, Date endDate) {
		if (startDate.after(endDate)) {
			throw new IllegalArgumentException(
					"End date should be grater or equals to start date");
		}

		long startDateTime = startDate.getTime();
		long endDateTime = endDate.getTime();
		long milPerDay = 1000 * 60 * 60 * 24;

		int numOfDays = (int) ((endDateTime - startDateTime) / milPerDay); // calculate
																			// vacation
																			// duration
																			// in
																			// days

		return (numOfDays + 1); // add one day to include start date in interval
	}

	/**
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static int calculateNumberOfWeekendsInRange(Date startDate,
			Date endDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);

		int counter = 0;
		while (!calendar.getTime().after(endDate)) {
			int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
			if (dayOfWeek == 1 || dayOfWeek == 7) {
				counter++;
			}
			calendar.add(Calendar.DAY_OF_MONTH, 1);
		}

		return counter;
	}

	/**
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public int calculateNumberOfDaysExcludeWeekends(Date startDate, Date endDate) {
		if (startDate.after(endDate)) {
			throw new IllegalArgumentException(
					"End date should be grater or equals to start date");
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);

		int numOfDays = 0;
		while (!calendar.getTime().after(endDate)) {
			int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
			if ((dayOfWeek > 1) && (dayOfWeek < 7)) {
				numOfDays++;
			}
			calendar.add(Calendar.DAY_OF_MONTH, 1);
		}

		return numOfDays;
	}
	
	public static Date MonthBefore(Date date, int month){
		  Calendar calendar = Calendar.getInstance();
		  calendar.setTime(date);
		  calendar.add(Calendar.MONTH, -month);
		  return calendar.getTime();
		 }

}
