package io.gnews.pro.core.model.dto;

import java.util.List;

/**
 * @author masasdani
 * Created Date Nov 3, 2015
 * @param <T>
 */
public class ListCounter<T> {

	private int count;
	private List<T> list;

	public ListCounter() {

	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

}
