package io.gnews.pro.core.model.mongodb;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import io.gnews.pro.core.config.CascadeSave;

@SuppressWarnings("serial")
@Document(collection = "agent_account")
public class AgentAccount implements Serializable {

	@Id
	private String id;
	@DBRef
	@CascadeSave
	private Account account;
	@Indexed(unique = true)
	private String codename;
	private String fullname;
	private String company;
	private String address;
	private String country;
	private String profilePicture;
	private long defaultPrice = 0;
	
	public AgentAccount() {
	
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCodename() {
		return codename;
	}

	public void setCodename(String codename) {
		this.codename = codename;
	}

	public String getFullname() {
		return fullname;
	}
	
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public long getDefaultPrice() {
		return defaultPrice;
	}

	public void setDefaultPrice(long defaultPrice) {
		this.defaultPrice = defaultPrice;
	}
	
}
