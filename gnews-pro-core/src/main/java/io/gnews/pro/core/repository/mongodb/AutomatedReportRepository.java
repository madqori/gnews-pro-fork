package io.gnews.pro.core.repository.mongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.AutomatedReport;
import io.gnews.pro.core.model.mongodb.Project;


public interface AutomatedReportRepository extends MongoRepository<AutomatedReport, String>{
	
	public AutomatedReport findByProject(Project project);
	
	public AutomatedReport findByProjectId(String id);
	
	public AutomatedReport findByActive(boolean active);
	
	public List<AutomatedReport> findByActiveTrueAndDayAndHour(String day, int hour);

	public List<AutomatedReport> findByActiveTrueAndMonthlyCount(int week);

}
