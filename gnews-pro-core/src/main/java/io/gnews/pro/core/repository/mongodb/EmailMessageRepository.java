package io.gnews.pro.core.repository.mongodb;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.EmailMessage;

public interface EmailMessageRepository extends MongoRepository<EmailMessage, String> {

	public List<EmailMessage> findByProjectId(String projectId);

	public Page<EmailMessage> findByProjectId(String projectId, Pageable pageable);

}
