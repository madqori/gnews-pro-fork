package io.gnews.pro.core.repository.mongodb;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.MediaRankingGlobal;

public interface MediaRankGlobalRepository extends MongoRepository<MediaRankingGlobal, String>{

}
