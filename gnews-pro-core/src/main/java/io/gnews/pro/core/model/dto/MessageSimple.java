package io.gnews.pro.core.model.dto;

import java.util.Date;
import java.util.List;

public class MessageSimple {

	private String messageId;
	private String emailFrom;
	private List<String> emailTo;
	private List<String> emailCc;
	private List<String> emailBcc;
	private String subject;
	private String content;
	private List<String> attachments;
	private Date sendDate;
	
	public MessageSimple() {
	
	}
	
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public List<String> getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(List<String> emailTo) {
		this.emailTo = emailTo;
	}

	public List<String> getEmailCc() {
		return emailCc;
	}

	public void setEmailCc(List<String> emailCc) {
		this.emailCc = emailCc;
	}

	public List<String> getEmailBcc() {
		return emailBcc;
	}

	public void setEmailBcc(List<String> emailBcc) {
		this.emailBcc = emailBcc;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public List<String> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<String> attachments) {
		this.attachments = attachments;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	
}
