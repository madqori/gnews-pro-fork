package io.gnews.pro.core.model.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.common.base.Throwables;

import io.gnews.pro.core.model.mongodb.WebData;
import io.gnews.pro.core.model.rest.AnalyticKeywordForm;
import io.gnews.pro.core.model.rest.ResponseData;
import io.gnews.pro.core.model.service.GnewsRestService;

@Service
public class GnewsRestServiceImpl implements GnewsRestService {

	private Logger log = LoggerFactory.getLogger(getClass());

	public static final String BASE_URL 					= "http://api.gnews.io";
	public static final String RECENT_ARTICLE 				= "/analytic/news/raw";
	public static final String ANALYTIC_KEYWORD 			= "/analytic/keyword";
	public static final String ANALYTIC_KEYWORD_IMAGE 		= "/analytic/keyword/image";
	
    @Autowired
    private RestTemplate restTemplate;

	@Value("${gnews.client.id}")
	private String clientId;

	@Value("${gnews.client.secret}")
	private String clientSecret;

	@Override
	public boolean createAnalyticKeyword(String keyword) {
		String url = BASE_URL+ANALYTIC_KEYWORD;
		AnalyticKeywordForm form = new AnalyticKeywordForm();
		form.setClientId(clientId);
		form.setClientSecret(clientSecret);
		form.setKeyword(keyword);
		HttpEntity<AnalyticKeywordForm> httpEntity = new HttpEntity<>(form);
		ResponseData<Object> responseData = restTemplate.exchange(url, HttpMethod.POST, httpEntity,
				new ParameterizedTypeReference<ResponseData<Object>>() {
				}).getBody();
		return responseData.getStatus().equalsIgnoreCase("success");
	}

	@Override
	public String getKeywordImage(String keyword) {
		String url = BASE_URL+ANALYTIC_KEYWORD_IMAGE;
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromHttpUrl(url)
                .queryParam("clientId", clientId)
                .queryParam("clientSecret", clientSecret)
                .queryParam("keyword", keyword);
		log.debug(url);
        try{
			ResponseData<String> responseData = restTemplate.exchange(
                    uriBuilder.toUriString(),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<ResponseData<String>>() {}).getBody();
        	return responseData.getData();
        }catch(Exception exception){
        	log.error(Throwables.getStackTraceAsString(exception));
        }
		return null;
	}

	@Override
	public List<WebData> getData(String keyword, 
			String language,
			String filter,
			Integer limit, 
			Integer offset 
			) {
		String url = BASE_URL+RECENT_ARTICLE;
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromHttpUrl(url)
                .queryParam("clientId", clientId)
                .queryParam("clientSecret", clientSecret)
                .queryParam("keyword", keyword)
				.queryParam("language", language)
				.queryParam("filter", filter)
				.queryParam("limit", limit)
				.queryParam("offset", offset);
		log.info("this url :" + uriBuilder);
		log.debug(uriBuilder.toUriString());
        try{
        	ResponseData<List<WebData>> responseData = restTemplate.exchange(
        			uriBuilder.build().toUri(),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<ResponseData<List<WebData>>>() {}).getBody();
        	return responseData.getData();
        }catch(Exception exception){
        	log.error(Throwables.getStackTraceAsString(exception));
        }
        return null;
	}

	public Object post(String url, Object data){
		return restTemplate.postForObject(url, data, Object.class);
	}

	
}
