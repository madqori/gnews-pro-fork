package io.gnews.pro.core.repository.mongodb;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.AgentAccount;
import io.gnews.pro.core.model.mongodb.PromoCode;

//@SuppressWarnings("unused")
public interface PromoCodeRepository extends MongoRepository<PromoCode, String>{

	public PromoCode findByCode(String code);
	
	public PromoCode findByCodeAndSubmitedFalse(String code);
	
	public Long countByAgent(AgentAccount agent);
	
	public List<PromoCode> findCodeByAgentAndSubmited(AgentAccount agent, boolean submited);

	public List<PromoCode> findCodeByAgentAndExpired(AgentAccount agent, boolean submited);
	
	public List<PromoCode> findCodeByExpiredAt(Date expired);
}
