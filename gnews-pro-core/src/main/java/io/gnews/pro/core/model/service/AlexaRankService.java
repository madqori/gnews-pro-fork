package io.gnews.pro.core.model.service;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * @author masasdani 
 * Created Date Nov 3, 2015
 */
@Service
public class AlexaRankService {

	public static final String ALEXA_BASE_URL = "http://data.alexa.com/data?cli=10&url=";

	private Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * @param domain
	 * @return
	 */
	public AlexaResult getResult(String domain) {
		AlexaResult result = new AlexaResult();
		result.setDomain(domain);
		String url = ALEXA_BASE_URL + domain;
		try {
			URLConnection conn = new URL(url).openConnection();
			InputStream is = conn.getInputStream();

			DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document doc = dBuilder.parse(is);
			Element element = doc.getDocumentElement();
			// popularity rank
			NodeList nodePopularity = element
					.getElementsByTagName("POPULARITY");
			if (nodePopularity.getLength() > 0) {
				Element elementAttribute = (Element) nodePopularity.item(0);
				String ranking = elementAttribute.getAttribute("TEXT");
				if (!"".equals(ranking)) {
					result.setPopularityRank(Integer.valueOf(ranking));
				}
			}
			// reach rank
			NodeList nodeReach = element.getElementsByTagName("REACH");
			if (nodeReach.getLength() > 0) {
				Element elementAttribute = (Element) nodeReach.item(0);
				String ranking = elementAttribute.getAttribute("RANK");
				if (!"".equals(ranking)) {
					result.setReachRank(Integer.valueOf(ranking));
				}
			}
			// country rank
			NodeList nodeCountry = element.getElementsByTagName("COUNTRY");
			if (nodeCountry.getLength() > 0) {
				Element elementAttribute = (Element) nodeCountry.item(0);
				String name = elementAttribute.getAttribute("NAME");
				if (!"".equals(name)) {
					result.setCountryName(name);
				}
				String ranking = elementAttribute.getAttribute("RANK");
				if (!"".equals(ranking)) {
					result.setCountryRank(Integer.valueOf(ranking));
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return result;
	}

	/**
	 * @author masasdani
	 * Created Date Nov 4, 2015
	 */
	public class AlexaResult {

		private String domain;
		private int popularityRank = 0;
		private int reachRank = 0;
		private String countryName = "";
		private int countryRank = 0;

		public AlexaResult() {

		}

		public String getDomain() {
			return domain;
		}

		public void setDomain(String domain) {
			this.domain = domain;
		}

		public int getPopularityRank() {
			return popularityRank;
		}

		public void setPopularityRank(int popularityRank) {
			this.popularityRank = popularityRank;
		}

		public int getReachRank() {
			return reachRank;
		}

		public void setReachRank(int reachRank) {
			this.reachRank = reachRank;
		}

		public int getCountryRank() {
			return countryRank;
		}

		public void setCountryRank(int countryRank) {
			this.countryRank = countryRank;
		}
		
		public String getCountryName() {
			return countryName;
		}

		public void setCountryName(String countryName) {
			this.countryName = countryName;
		}

		@Override
		public String toString() {
			return "domain : " + domain + 
					", popularity : " + popularityRank + 
					", reach : " + reachRank + 
					", country : " + countryName + 
					", country rank : " + countryRank;
		}
	}
}
