package io.gnews.pro.core.model.service;

import io.gnews.pro.core.model.mongodb.AgentAccount;
import io.gnews.pro.core.model.mongodb.UserAccount;

public interface UserUtilService {
	
	public UserAccount findUserByEmail(String email);
	
	public AgentAccount findAgentByEmail(String email);
	
	
}
