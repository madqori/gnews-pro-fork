package io.gnews.pro.crawler.model.rest;

/**
 * @author masasdani
 * Created Date Oct 4, 2015
 * @param <T>
 */
public class ResponseDataDocument {

	private String status;
	private String message;
	private Document data;
	
	public ResponseDataDocument() {
	
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Document getData() {
		return data;
	}

	public void setData(Document data) {
		this.data = data;
	}
	
}
