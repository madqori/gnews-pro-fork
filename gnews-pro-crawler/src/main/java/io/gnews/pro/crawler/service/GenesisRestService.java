package io.gnews.pro.crawler.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import io.gnews.pro.crawler.model.rest.MentalisApiForm;
import io.gnews.pro.crawler.model.rest.MentalisApiTokenizerForm;
import io.gnews.pro.crawler.model.rest.ResponseDataDocument;
import io.gnews.pro.crawler.model.rest.ResponseDataTokenize;

/**
 * @author masasdani
 * Created Date Nov 9, 2015
 */
@Service
public class GenesisRestService {

	private Logger log = LoggerFactory.getLogger(getClass());
	
	public static final String BASE_URL = "http://gdilab.com:10080";
	public static final String ENDPOINT_MENTALIS = "/api/mentalis";
	public static final String ENDPOINT_TOKENIZE = "/api/mentalis/tokenize";
	
	public static final String PARAM_ACCESS_TOKEN = "?accessToken=lzjsfsb98w46vk1n4ytq5js6n3tk73o4";
	public static final String PARAM_TEXT = "&text=";
	
	@Value("${genesis.accessToken}")
	private String accessToken;
	
	@Autowired
	private RestTemplate restTemplate;
	
	public ResponseDataDocument getSentimentAnalysis(String text){
		return getSentimentAnalysis(text, null);
	}
	
	public ResponseDataDocument getSentimentAnalysis(String text, String language){
		try{
			MentalisApiForm form = new MentalisApiForm();
			form.setAccessToken("lzjsfsb98w46vk1n4ytq5js6n3tk73o4");
			form.setText(text);
			form.setLanguage(language);;
			String url = BASE_URL+ENDPOINT_MENTALIS;
			ResponseDataDocument response = restTemplate.postForObject(url, form, ResponseDataDocument.class);
			return response;
		}catch(Exception e){
			log.error(e.getMessage());
			return null;
		}
	}

	public ResponseDataTokenize getTokenizeText(String text){
		return getTokenizeText(text, null);
	}
		
	public ResponseDataTokenize getTokenizeText(String text, String language){
		try{
			MentalisApiTokenizerForm form = new MentalisApiTokenizerForm();
			form.setAccessToken("lzjsfsb98w46vk1n4ytq5js6n3tk73o4");
			form.setText(text);
			form.setLanguage(language);;
			String url = BASE_URL+ENDPOINT_TOKENIZE;
			ResponseDataTokenize response = restTemplate.postForObject(url, form, ResponseDataTokenize.class);
			return response;
		}catch(Exception e){
			log.error(e.getMessage());
			return null;
		}
	}
	
}
