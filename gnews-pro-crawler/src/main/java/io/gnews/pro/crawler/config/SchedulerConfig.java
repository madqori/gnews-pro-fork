package io.gnews.pro.crawler.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author madqori
 * Created Date May 3, 2016
 */
@Configuration
@EnableScheduling
public class SchedulerConfig {

}
