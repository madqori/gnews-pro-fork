package io.gnews.pro.crawler.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.config.AppConfig;

@Service
public class SchedulerService {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CrawlerService crawlService;
	
	@Scheduled(cron = "0 0 0 * * ?")
	public void updateData(){
		log.info("starting scheduled update for all projects");
		crawlService.updateAllProjects(AppConfig.TYPE_UPDATE[1]);
	}
	
	@Scheduled(cron = "0 0 6 * * ?")
	public void udpateKeywordMapping(){
		log.info("starting update keyword mapping");
		crawlService.unUpdatedKeywordMapping();
	}
	
}
	

