package io.gnews.pro.crawler.repository;

import java.util.Date;
import java.util.List;

import io.gnews.pro.core.model.dto.ArticleData;
import io.gnews.pro.core.model.dto.PeakCounter;
import io.gnews.pro.core.model.dto.SentimentCounter;
import io.gnews.pro.core.model.dto.SentimentPeakCounter;
import io.gnews.pro.core.model.dto.TermCounter;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.model.mongodb.WebData;

/**
 * @author masasdani 
 * Created Date Nov 3, 2015
 */
public interface WebDataCrawlerRepository {

	/**
	 * @param project
	 * @param url
	 */
	public WebData getWebData(Project project, String url);
	
	/**
	 * @param project
	 * @param webData
	 */
	public void saveWebData(Project project, WebData webData);

	/**
	 * @param project
	 * @return
	 */
	public Date getLastNewsDate(Project project);
	
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Long getMediaCount(Project project, Date startDate,
			Date endDate);

	
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Long getArticleCount(Project project, Date startDate,
			Date endDate);
	
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<TermCounter> getMostMedia(Project project, Date startDate,
			Date endDate, int limit, int offset);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<TermCounter> getMostMediaPositive(Project project, Date startDate,
			Date endDate, int limit, int offset);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<TermCounter> getMostMediaNegative(Project project, Date startDate,
			Date endDate, int limit, int offset);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<TermCounter> getMostMediaNeutral(Project project, Date startDate,
			Date endDate, int limit, int offset);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PeakCounter> getPeakDay(Project project, Date startDate,
			Date endDate);
	
	/**
	 * @param project
	 * @param id
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<ArticleData> getPeakDayById(Project project, Long id, Date startDate,
			Date endDate);
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PeakCounter> getPeakHour(Project project, Date startDate,
			Date endDate);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public SentimentCounter getSentimentCounter(Project project, Date startDate,
			Date endDate);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<ArticleData> getArticlePositive(Project project, Date startDate,
			Date endDate, int limit, int offset);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<ArticleData> getArticleNegative(Project project, Date startDate,
			Date endDate, int limit, int offset);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<ArticleData> getArticleNeutral(Project project, Date startDate,
			Date endDate, int limit, int offset);

	/**
	 * @param project
	 * @param peakDay
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<ArticleData> getArticleByPeakDay(Project project, Long peakDay,
			int limit, int offset);

	/**
	 * @param project
	 * @param peakHour
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<ArticleData> getArticleByPeakHour(Project project, Long peakHour,
			int limit, int offset);

	/**
	 * @param project
	 * @param peakHour
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<ArticleData> getArticleByTerm(Project project, String term,
			int limit, int offset);

	/**
	 * @param project
	 */
	public void removeProject(Project project);

	/**
	 * @param project
	 * @param terms
	 * @param startDate
	 * @param endDate
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<ArticleData> getMostMediaByDomain(Project project, String terms, Date startDate, Date endDate, int limit,
			int offset);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */

	public List<SentimentPeakCounter> getSentimentPositiveEachDay(Project project, Date startDate, Date endDate);
	
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<SentimentPeakCounter> getSentimentNegativeEachDay(Project project, Date startDate, Date endDate);
	
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<SentimentPeakCounter> getSentimentNeutralEachDay(Project project, Date startDate, Date endDate);

	/**
	 * @param project
	 * @param term
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Long getArticleCountByTerm(Project project, String term, Date startDate, Date endDate);

	/**
	 * @param project
	 * @param peakDay
	 * @param sentiment
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<ArticleData> getArticleSentimentByPeakDay(Project project, Long peakDay, String sentiment,
			Integer limit, Integer offset);

	/**
	 * @param project
	 * @param peakHour
	 * @return
	 */
	public Long getPeakHourMediaCount(Project project, Date startDate, Date endDate, Long peakHour);

	/**
	 * @param project
	 * @param term
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Long getMediaCountByTerm(Project project, String term, Date startDate, Date endDate);

	/**
	 * @param project
	 * @return
	 */
	public void deleteArticleByProjectId(Project project, String urlId);
	
	/**
	 * @param project
	 * @param urlId
	 * @return 
	 */
	public ArticleData editArticleByProjectId(Project project, String urlId);
	
	/**
	 * @param project
	 * @param urlId
	 * @return
	 */
	public ArticleData getSingleArticleById(Project project, String urlId);

	/**
	 * @param project
	 * @param start
	 * @param end
	 * @return
	 */
	public List<TermCounter> getKeywordMappingData(Project project, Date start, Date end);

	/**
	 * @param project
	 * @param start
	 * @param end
	 */
	public void saveKeywordMapping(Project project, Date start, Date end);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 */
	public void updateMostPopularMediaRanking(Project project, Date startDate, Date endDate);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 */
	
	public void UnUpdatedKeywordMapping(Project project, Date startDate, Date endDate);
}
