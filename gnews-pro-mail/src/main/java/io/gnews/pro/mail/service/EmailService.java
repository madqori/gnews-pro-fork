package io.gnews.pro.mail.service;

import io.gnews.pro.core.model.dto.SendEmailForm;

public interface EmailService {

	/**
	 * @param form
	 */
	public void sendEmail(SendEmailForm form);
	
	/**
	 * @param to
	 * @param subject
	 * @param content
	 * @param attachment
	 */
	public void sendARMail(String to, String subject, String content,String attachment);
		

}
