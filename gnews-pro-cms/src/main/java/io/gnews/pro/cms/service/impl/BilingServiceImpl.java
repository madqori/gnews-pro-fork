package io.gnews.pro.cms.service.impl;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gnews.pro.cms.model.request.BillingForm;
import io.gnews.pro.cms.model.response.BillingData;
import io.gnews.pro.cms.service.BillingService;
import io.gnews.pro.core.model.mongodb.AgentAccount;
import io.gnews.pro.core.model.mongodb.Billing;
import io.gnews.pro.core.repository.mongodb.BillingRepository;

@Service
public class BilingServiceImpl implements BillingService {

	@Autowired
	private BillingRepository billingRepository;
	
	@Override
	public void save(Principal principal, BillingForm form) {
	/*	Account account = accountRepository.findByEmail(form.getAgentEmail());
		Billing billing = new Billing();
		if (form.getId() != null && form.getId().length()>0)
			billing.setId(form.getId());
		billing.setCreatedAt(new Date());
		billing.setPeriod(form.getPeriod());	
		billing.setAgent(account);
		billing.setAmount(form.getAmount());
		billing.setPaid(false);
		billingRepository.save(billing);*/
	}
	
	@Override
	public List<BillingData> getBillingData(Principal principal) {
		List<BillingData> billingDatas = new ArrayList<>();
		List<Billing> list= billingRepository.findAll();
		
		for(Billing billing: list){
			BillingData billingData = new BillingData();
			billingData.setId(billing.getId());
			billingData.setCreatedAt(billing.getCreatedAt());
			billingData.setPeriod(billing.getPeriod());
			billingData.setAgent(billing.getAgent().getAccount().getEmail());
			billingData.setAmount(billing.getAmount());
			billingData.setPaidOff(billing.isPaid());
			billingDatas.add(billingData);	
		}
		return billingDatas;
	}

	@Override
	public BillingData getBillingById(String id) {
		Billing billing = billingRepository.findOne(id);
		
		if (billing == null){
			return null;
		}
		BillingData billingData = new BillingData();
		billingData.setId(billing.getId());
		billingData.setAgent(billing.getAgent().getAccount().getEmail());
		billingData.setPeriod(billing.getPeriod());
		billingData.setAmount(billing.getAmount());
		
		return billingData;
	}

	@Override
	public void delete(String id) {
		billingRepository.delete(id);	
	}

	@Override
	public List<AgentAccount> getAllAgent() {
		/*return	accountRepository.findAll();*/
		return null;
	}

	@Override
	public List<BillingData> getBillingForAgent(Principal principal) {
		/*List<BillingData> billingDatas = new ArrayList<>();
		List<Billing> list= billingRepository.findAll();
		AgentAccount agentAccount =  accountRepository.findByAccountEmail(principal.getName());
		for(Billing billing: list){
			if(billing.getAgent().equals(agentAccount)){
			BillingData billingData = new BillingData();
			billingData.setId(billing.getId());
			billingData.setCreatedAt(billing.getCreatedAt());
			billingData.setPeriod(billing.getPeriod());
			billingData.setAgent(billing.getAgent().getAccount().getEmail());
			billingData.setAmount(billing.getAmount());
			billingData.setPaidOff(billing.isPaid());
			billingDatas.add(billingData);
			}else{
				continue;}
		}
		return billingDatas;
	}*/
		return null;
	}
}
