package io.gnews.pro.cms.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.gnews.pro.cms.service.DasboardService;
import io.gnews.pro.cms.service.SiteService;
import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.repository.mongodb.AccountRepository;

@Controller
@RequestMapping(value = "/")
public class DashboardController {
	
	Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	SiteService siteService;
	
	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	DasboardService dasboardService;
	
	@RequestMapping(method = RequestMethod.GET, value ="home")
	public String index(Principal principal, Model model){		
		if (principal == null){
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		log.info(principal.getName());
		Account account = accountRepository.findByEmail(principal.getName());
		
		model.addAttribute("recurrent",dasboardService.countRecurrent(principal));
		model.addAttribute("codeGenerated", dasboardService.countCodeGenerated(principal));
		model.addAttribute("codeSubmited", dasboardService.countCodeSubmited(principal));
		model.addAttribute("codeExpired", dasboardService.countCodeExpired(principal));
		model.addAttribute("project", dasboardService.countClientproject(principal));
		model.addAttribute("sidebar",account.getType());
		log.info(account.getType());
		
		return "home";
	}

}

