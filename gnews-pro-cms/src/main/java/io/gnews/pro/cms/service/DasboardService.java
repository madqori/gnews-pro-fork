package io.gnews.pro.cms.service;

import java.security.Principal;

public interface DasboardService {

	public Long countRecurrent(Principal principal);

	public Long countCodeGenerated(Principal principal);

	public int countCodeSubmited(Principal principal);

	public int countCodeExpired(Principal principal);

	public int countClientproject(Principal principal);

}
