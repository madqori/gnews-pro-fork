package io.gnews.pro.cms.service.impl;

import java.security.Principal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gnews.pro.cms.model.request.PromoCodeForm;
import io.gnews.pro.cms.model.response.PromoCodeData;
import io.gnews.pro.cms.service.PromoCodeService;
import io.gnews.pro.core.model.mongodb.PromoCode;
import io.gnews.pro.core.repository.mongodb.AgentAccountRepository;
import io.gnews.pro.core.repository.mongodb.PromoCodeRepository;

@Service
public class PromoCodeServiceImpl implements PromoCodeService{
	
	@Autowired
	PromoCodeRepository createCodeRepository;
	
	@Autowired
	AgentAccountRepository accountRepository;
	
	@Override
	public void save(Principal principal, PromoCodeForm form) {
		/*AgentAccount account = accountRepository.findByAccountEmail(principal.getName());
		PromoCode code = new PromoCode();
		if (form.getId() != null && form.getId().length()>0)
			code.setId(form.getId());
		code.setCode(form.getCodename());
		code.setPrice(form.getPrice());
		code.setPeriod(form.getPeriod());
		code.setExpiredAt(form.getExpiredDate());
		code.setCreatedAt(new Date());
		code.setAgent(account);
		createCodeRepository.save(code);*/
	}
	
	@Override
	public List<PromoCodeData> getCreateCodeData(Principal principal) {
	/*	List<PromoCodeData> codeDatas = new ArrayList<>();
		List<PromoCode> list = createCodeRepository.findAll();
		AgentAccount account = accountRepository.findByAccountEmail(principal.getName());
		for(PromoCode code : list){			
			if(code.getAgent().equals(account)){
			PromoCodeData codeData = new PromoCodeData();
			codeData.setId(code.getId());
			codeData.setCodename(code.getCode());
			codeData.setPrice(code.getPrice());
			codeData.setPeriod(code.getPeriod());
			codeData.setSubmited(code.isSubmited());
			codeData.setExpiredDate(code.getExpiredAt());
			codeData.setCreatedDate(code.getCreatedAt());
			codeDatas.add(codeData);
		}else{
			continue;
			}
		}
		return codeDatas;*/
		return null;
	}

	@Override
	public PromoCodeData getCreateCodeById(String id) {
		PromoCode code = createCodeRepository.findOne(id);
		
		if (code == null){
			return null;
		}
		PromoCodeData codeData = new PromoCodeData();
		codeData.setId(code.getId());
		codeData.setCodename(code.getCode());
		codeData.setPrice(code.getPrice());
		codeData.setExpiredDate(code.getExpiredAt());
		
		return codeData;
	}

	@Override
	public void delete(String id) {
		createCodeRepository.delete(id);	
	}

	@Override
	public String GetCodename(Principal principal) {
	/*	AgentAccount account = accountRepository.findByAccountEmail(principal.getName());
		log.info(account.getCodename());
		return account.getCodename();	
	 */
		return null;
	}
}
