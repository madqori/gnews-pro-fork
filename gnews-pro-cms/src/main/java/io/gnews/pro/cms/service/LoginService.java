package io.gnews.pro.cms.service;

import java.util.Collections;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.model.mongodb.AgentAccount;
import io.gnews.pro.core.repository.mongodb.AccountRepository;
import io.gnews.pro.core.repository.mongodb.AgentAccountRepository;

/**
 * @author masasdani
 * Created Date Oct 28, 2015
 */
@Service
public class LoginService implements UserDetailsService,
		ApplicationListener<AuthenticationSuccessEvent> {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AccountRepository accountRepository; 
	
	@Autowired
	private AgentAccountRepository agentAccountRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@PostConstruct
	public void initialize() {
		long size = accountRepository.countByType(Account.TYPE_ADMIN);
		if (size == 0) {
			Account account = new Account(
					"admin@gnews.io", 
					true,
					"gnewsadmin",
					passwordEncoder.encode("admin"));
			account.setType(Account.TYPE_ADMIN);
			account = accountRepository.save(account);
			
			AgentAccount agentAccount = new AgentAccount();
			agentAccount.setCodename("admin");
			agentAccount.setAccount(account);
			agentAccountRepository.save(agentAccount);	
		}
	}

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		Account account = accountRepository.findByEmailAndActiveTrue(username);
		if (account == null || account.getType().equals(Account.TYPE_USER)) {
			throw new UsernameNotFoundException("user not found");
		}
		return createUser(account);
	}

	/**
	 * @param account
	 */
	public void signin(Account account) {
		SecurityContextHolder.getContext().setAuthentication(
				authenticate(account));
	}

	/**
	 * @param account
	 * @return
	 */
	private Authentication authenticate(Account account) {
		return new UsernamePasswordAuthenticationToken(createUser(account),
				null, Collections.singleton(createAuthority(account)));
	}

	/**
	 * @param account
	 * @return
	 */
	private org.springframework.security.core.userdetails.User createUser(
			Account account) {
		return new org.springframework.security.core.userdetails.User(
				account.getEmail(), account.getPassword(),
				Collections.singleton(createAuthority(account)));
	}

	/**
	 * @param account
	 * @return
	 */
	private GrantedAuthority createAuthority(Account account) {
		return new SimpleGrantedAuthority(account.getType());
	}

	@Override
	public void onApplicationEvent(AuthenticationSuccessEvent event) {
		String userName = ((UserDetails) event.getAuthentication()
				.getPrincipal()).getUsername();
		Account account = accountRepository.findByEmail(userName);
		account.setLastLogin(new Date());
		
	}

}
