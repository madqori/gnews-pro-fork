package io.gnews.pro.cms.service.impl;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import io.gnews.pro.cms.model.request.UserForm;
import io.gnews.pro.cms.model.response.UserData;
import io.gnews.pro.cms.service.UserService;
import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.model.mongodb.UserAccount;
import io.gnews.pro.core.repository.mongodb.AccountRepository;
import io.gnews.pro.core.repository.mongodb.UserAccountRepository;

@Service
public class UserServiceImpl implements UserService{
	
	Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private UserAccountRepository userAccountRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public void save(Principal principal,UserForm form) {
		Account account = new Account();
		UserAccount user = new UserAccount();
		
		if (form.getAccountId() != null && form.getAccountId().length()>0)
			account.setId(form.getAccountId());
		if(form.getUserId() != null && form.getUserId().length()>0)
			user.setId(form.getUserId());
		
		account.setEmail(form.getEmail());
		account.setCreatedAt(new Date());
		account.setActive(true);
		account.setPassword(passwordEncoder.encode(form.getPassword()));
		account.setActive(true);
		account.setType(Account.TYPE_USER);
		accountRepository.save(account);
		
		user.setFullname(form.getFullname());
		user.setCompany(form.getCompany());
		user.setAddress(form.getAddress());
		user.setAccount(account);
		user.setRecurrentUser(true);
		userAccountRepository.save(user);
	}
	
	
	@Override
	public List<UserData> getUserData(Principal principal) {
		List<Account> accounts = accountRepository.findByType(Account.TYPE_USER);
		List<UserData> listUser = new ArrayList<>();
		for(Account account : accounts){
			UserAccount user = userAccountRepository.findByAccount(account);
			
			UserData userData = new UserData();
			userData.setAccountId(account.getId());
			userData.setEmail(account.getEmail());
			userData.setFullname(user.getFullname());
			userData.setActive(account.isActive());
			userData.setCompany(user.getCompany());
			userData.setAddress(user.getAddress());
			
			listUser.add(userData);
		}		
		return listUser;
	}

	@Override
	public UserData getUserById(String id) {
		Account account = accountRepository.findOne(id);
		UserAccount user = userAccountRepository.findByAccount(account);
		
		if (user == null){
			return null;
		}
		
		UserData userData = new UserData();
		userData.setAccountId(account.getId());
		userData.setUserId(user.getId());
		userData.setEmail(account.getEmail());
		userData.setFullname(user.getFullname());
		userData.setCompany(user.getCompany());
		userData.setAddress(user.getAddress());
		
		return userData;
	}
	
	@Override
	public void markActive(String id){
			UserAccount account = userAccountRepository.findOne(id);
			account.getAccount().setActive(true);
			userAccountRepository.save(account);
	
		}
	
	@Override
	public void markSuspend(String id){
			UserAccount account = userAccountRepository.findOne(id);
			account.getAccount().setActive(false);
			userAccountRepository.save(account);
	}

	@Override
	public void delete(String id) {
		userAccountRepository.delete(id);
	}
}
