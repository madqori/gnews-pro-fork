package io.gnews.pro.cms.service;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.repository.mongodb.AccountRepository;

/**
 * @author masasdani
 * Created Date Oct 28, 2015
 */
@Service
public class SiteService {

	public static final String DEFAULT_SITE_TITLE = "gnews.io";
	public static final String DEFAULT_SITE_META = "gnews homepage content mana";

	@Autowired
	private AccountRepository accountRepository;

	/**
	 * @param principal
	 * @param model
	 */
	public void setSiteInfo(Principal principal, Model model) {
		if (principal != null) {
			Account account = accountRepository
					.findByEmail(principal.getName());
			if (account == null) {
				model.addAttribute("principal", principal.getName());
			} else {
				model.addAttribute("principal", account.getEmail());
			}
		}
		model.addAttribute("title", DEFAULT_SITE_TITLE);
		model.addAttribute("meta", DEFAULT_SITE_META);
	}

	/**
	 * @param model
	 */
	public void setSiteInfo(Model model) {
		model.addAttribute("title", DEFAULT_SITE_TITLE);
		model.addAttribute("meta", DEFAULT_SITE_META);
	}

}
