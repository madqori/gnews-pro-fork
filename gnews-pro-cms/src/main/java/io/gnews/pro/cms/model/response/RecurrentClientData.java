package io.gnews.pro.cms.model.response;

public class RecurrentClientData {
	
	public Long id;
	public String email;
	public String fullname;
	public String company;
	public String address;
	public String password;
	public String identifyAgent;
	
	public RecurrentClientData() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIdentifyAgent() {
		return identifyAgent;
	}

	public void setIdentifyAgent(String identifyAgent) {
		this.identifyAgent = identifyAgent;
	}
}
