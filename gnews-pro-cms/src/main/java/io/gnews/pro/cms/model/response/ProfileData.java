package io.gnews.pro.cms.model.response;

public class ProfileData {
	
	private String accountId;
	private String userId;
	private String username;
	private String fullname;
	private Long defaultPrice;
	private String email;
	private String password;
	
	public ProfileData() {
		
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public Long getDefaultPrice() {
		return defaultPrice;
	}

	public void setDefaultPrice(Long defaultPrice) {
		this.defaultPrice = defaultPrice;
	}	
}
