package io.gnews.pro.cms.model.response;

public class DashboardData {

	int recurent;
	int generateCode;
	int codeSubmited;
	int codeExpired;
	int clientProject;

	public DashboardData() {
		// TODO Auto-generated constructor stub
	}

	public int getRecurent() {
		return recurent;
	}

	public void setRecurent(int recurent) {
		this.recurent = recurent;
	}

	public int getGenerateCode() {
		return generateCode;
	}

	public void setGenerateCode(int generateCode) {
		this.generateCode = generateCode;
	}

	public int getCodeSubmited() {
		return codeSubmited;
	}

	public void setCodeSubmited(int codeSubmited) {
		this.codeSubmited = codeSubmited;
	}

	public int getCodeExpired() {
		return codeExpired;
	}

	public void setCodeExpired(int codeExpired) {
		this.codeExpired = codeExpired;
	}

	public int getClientProject() {
		return clientProject;
	}

	public void setClientProject(int clientProject) {
		this.clientProject = clientProject;
	}

}
