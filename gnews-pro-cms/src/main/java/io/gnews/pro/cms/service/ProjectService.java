package io.gnews.pro.cms.service;


import java.security.Principal;
import java.util.List;

import org.springframework.stereotype.Service;

import io.gnews.pro.cms.model.response.ProjectData;

@Service
public interface ProjectService {

	/**
	 * @param principal
	 * @return
	 */
	public List<ProjectData> getProjectData(Principal principal);
}
