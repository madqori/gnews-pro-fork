package io.gnews.pro.cms.service;

import java.security.Principal;
import java.util.List;

import org.springframework.stereotype.Service;

import io.gnews.pro.cms.model.request.BillingForm;
import io.gnews.pro.cms.model.response.BillingData;
import io.gnews.pro.core.model.mongodb.AgentAccount;

@Service
public interface BillingService {

	/**
	 * @param principal
	 * @return
	 */
	public List<BillingData> getBillingData(Principal principal);
	
	/**
	 * @param principal
	 * @return
	 */
	public List<BillingData> getBillingForAgent(Principal principal);
	
	/**
	 * @param id
	 * @return
	 */
	public BillingData getBillingById(String id);
	
	/**
	 * @param id
	 */
	public void delete(String id);
	
	/**
	 * @return
	 */
	public List<AgentAccount> getAllAgent();

	/**
	 * @param principal
	 * @param form
	 */
	void save(Principal principal, BillingForm form);	
}
