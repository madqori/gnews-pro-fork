package io.gnews.pro.cms.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.gnews.pro.cms.model.request.ChangePasswordForm;
import io.gnews.pro.cms.model.request.DefaultPriceForm;
import io.gnews.pro.cms.model.response.ProfileData;
import io.gnews.pro.cms.service.SettingService;
import io.gnews.pro.cms.service.SiteService;
import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.repository.mongodb.AccountRepository;

@Controller
@RequestMapping(value = "/setting/")
public class SettingController {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SiteService siteService;

	@Autowired
	private SettingService settingService;
	
	@Autowired
	AccountRepository accountRepository;

	@RequestMapping(method = RequestMethod.GET, value = "/profile")
	public String index(Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		ProfileData profile = settingService.getProfileByEmail(principal.getName());
		model.addAttribute("profile", profile);
		
		return "profile";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/edit")
	public String edit(Principal principal, Model model, @RequestParam String id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		ProfileData profileData = settingService.getProfileById(id);
		model.addAttribute("profile", profileData);
		
		return "change-password";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/editprice")
	public String editprice(Principal principal, Model model, @RequestParam String id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		ProfileData profileData = settingService.getProfileById(id);
		model.addAttribute("priceprofile", profileData);
		
		return "change-default-price";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save")
	public String add(Principal principal, @ModelAttribute ChangePasswordForm form, Long id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		try {
			settingService.save(form, principal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "redirect:/setting/profile";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/saveprice")
	public String addDefaultPrice(Principal principal, @ModelAttribute DefaultPriceForm form, Long id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		settingService.savePrice(form, principal);
		
		return "redirect:/setting/profile";
	}

}
