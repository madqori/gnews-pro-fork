package io.gnews.pro.cms.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author masasdani
 * Created Date Oct 27, 2015
 */
@Configuration
@EnableScheduling
@EnableAsync
public class SchedulerConfig {

}
