package io.gnews.pro.cms.model.request;

public class DefaultPriceForm {

	private Long defaultPrice;

	public DefaultPriceForm() {
		// TODO Auto-generated constructor stub
	}

	public Long getDefaultPrice() {
		return defaultPrice;
	}

	public void setDefaultPrice(Long defaultPrice) {
		this.defaultPrice = defaultPrice;
	}	
}
